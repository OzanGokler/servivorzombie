﻿var bg;
var logo;
var backToMenuGroup;
var tweenLogo;
var boolMenu = false;
var menuTimer = 1;
var currentMenuPush;
var currentMenuPop;
var yes;
var no;
var btgText;
game.load.image("bg", "BackToGameMenu/background.png");
game.load.image("icon", "BackToGameMenu/phaserBoxLogo.png");
game.load.image("yes", "BackToGameMenu/yes.png");
game.load.image("no", "BackToGameMenu/no.png");
game.load.image("btgText", "BackToGameMenu/btgText.png");

function openBackToGameMenu()
{
    
    var camera_X = game.camera.position.x + game.width / 2;
    var camera_Y = game.camera.position.y + game.height / 2;

    if (boolMenu === false) {
        backToMenuGroup = game.add.group();
        bg = game.add.sprite(camera_X, camera_Y, "bg");
        bg.scale.setTo(0.5);
        bg.anchor.setTo(0.5);

        logo = game.add.sprite(camera_X, camera_Y - 80, "icon");
        logo.scale.setTo(0.5);
        logo.anchor.setTo(0.5);
        tweenLogo = game.add.tween(logo.scale).to({ x: 0.7, y: 0.7 }, 750, Phaser.Easing.Linear.In, true).loop(true);
        tweenLogo.yoyo(true);

        btgText = game.add.sprite(camera_X, camera_Y - 20, "btgText");
        btgText.scale.setTo(0.4);
        btgText.anchor.setTo(0.5);

        yes = game.add.sprite(camera_X - 100, camera_Y + 50, "yes");
        yes.scale.setTo(0.5);
        yes.anchor.setTo(0.5);
        yes.inputEnabled = true;
        yes.events.onInputUp.add(clickYES, this);

        no = game.add.sprite(camera_X + 100, camera_Y + 50, "no");
        no.scale.setTo(0.5);
        no.anchor.setTo(0.5);
        no.inputEnabled = true;
        no.events.onInputUp.add(clickNO, this);

        backToMenuGroup.add(bg);
        backToMenuGroup.add(logo);
        backToMenuGroup.add(btgText);
        backToMenuGroup.add(yes);
        backToMenuGroup.add(no);

        game.world.bringToTop(backToMenuGroup);
        boolMenu = true;
    }
}

function closeBackToMenu()
{
    if (boolMenu === true) {
        backToMenuGroup.destroy();
        boolMenu = false;
    }
    
}

function clickNO() {
    closeBackToMenu();
    game.paused = false;
}
function clickYES() {
    window.location.replace("file:///home/pi/Desktop/PhaserBox/index.html");
    console.log("menuuuuu");
}

var mousedownID = -1;  //Global ID of mouse down interval
function mousedown(event)
{
    if (mousedownID === -1)
    {
        //Prevent multimple loops!
        mousedownID = setInterval(whilemousedown, 50 /*execute every 50ms*/);
        currentMenuPush = game.time.totalElapsedSeconds();
    }  
}
function mouseup(event)
{
    if (mousedownID !== -1)
    {  //Only stop if exists
        clearInterval(mousedownID);
        mousedownID = -1;

    }
}
function whilemousedown()
{
    /*here put your code*/
    console.log(currentMenuPush + menuTimer, game.time.totalElapsedSeconds());
    if (currentMenuPush + menuTimer <= game.time.totalElapsedSeconds()) {
        openBackToGameMenu();
    }

    
}
//Assign events
document.addEventListener("mousedown", mousedown);
document.addEventListener("mouseup", mouseup);
//Also clear the interval when user leaves the window with mouse
document.addEventListener("mouseout", mouseup);

