﻿var Level = function () { };
Level.prototype = {
    bg: null,
    door: null,
    doorIsOpen: null,
    preload: function() {
        
    },
    create: function ()
    {
        this.doorIsOpen = false;
        _character.currentLevel = "level",
        _character.ItemKey = false;
        this.bg = game.add.sprite(0, 0, "bg_level");
        //_soundManager.levelMusic();
        this.door = game.add.sprite(200, 200, "door");
        game.physics.enable(this.door, Phaser.Physics.ARCADE);
        this.door.scale.setTo(3);
        this.door.animations.add("doorOpen", [0, 1, 2, 3]);
    },
    update: function() {
        
    }
}
var _level = new Level();