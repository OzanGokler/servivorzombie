﻿var LevelBoss2 = function () { };
LevelBoss2.prototype = {
    bg: null,
    muzic: null,
    door: null,
    doorIsOpen: null,
    preload: function () {
        
    },
    create: function ()
    {
        this.doorIsOpen = false;
        _character.currentLevel = "boss2",
        _character.ItemKey = false;
        this.bg = game.add.sprite(0, 0, "bg_boss1");
        this.door = game.add.sprite(100,100, "door");
        game.physics.enable(this.door, Phaser.Physics.ARCADE);
        this.door.scale.setTo(3);
        this.door.anchor.setTo(0.5);
        this.door.animations.add("doorOpen", [0, 1, 2, 3]);
        //_soundManager.levelMusicSound.stop();

        
        _soundManager.levelBoss2();
    },
    update: function ()
    {

    }
}
var _levelBoss2 = new LevelBoss2();