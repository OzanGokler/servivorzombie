﻿var Level1 = function () { };
Level1.prototype = {

    bg: null,
    door: null,
    doorIsOpen: null,
    preload: function ()
    {

    },
    create: function ()
    {
        _character.currentLevel = "level1",
        _character.ItemKey = false;
        this.doorIsOpen = false;

        for (var i = 0; i < 13; i++) {
            for (var j = 0; j < 17; j++) {
                game.add.sprite(200 * i, 150*j, "bg_level1");
            }
        }
        this.door = game.add.sprite(200, 200, "door");
        game.physics.enable(this.door, Phaser.Physics.ARCADE);
        this.door.scale.setTo(3);
        this.door.animations.add("doorOpen", [0, 1, 2, 3]);
    },
    update: function ()
    {

    }
}
var _level1 = new Level1();