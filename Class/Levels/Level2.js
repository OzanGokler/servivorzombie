﻿var Level2 = function () { };
Level2.prototype = {

    bg: null,
    door: null,
    doorIsOpen: null,
    preload: function ()
    {

    },
    create: function ()
    {
        this.doorIsOpen = false;
        _character.currentLevel = "level2",
        _character.ItemKey = false;

        for (var i = 0; i < 10; i++)
        {
            for (var j = 0; j < 10; j++)
            {
                game.add.sprite(512 * i, 512 * j, "bg_level2");
            }
        }
        this.door = game.add.sprite(0, 200, "door");
        game.physics.enable(this.door, Phaser.Physics.ARCADE);
        this.door.scale.setTo(3);
        this.door.animations.add("doorOpen", [0, 1, 2, 3]);
    },
    update: function ()
    {

    }
}
var _level2 = new Level2();