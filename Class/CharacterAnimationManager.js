﻿var AnimationManager = function () { };
AnimationManager.prototype = {

    //CHARACTER ANIMATIONS
    charIdle: function ()
    {
        _character.sprite.animations.play("idle", 20);
    },

    charWalk: function() {

        _character.sprite.animations.play("walk", 20);
    },
    charMelee: function ()
    {
        _character.sprite.animations.play("melee", 20);
        _character.sprite.events.onAnimationComplete.addOnce(this.charMeleeComplete, this);
    },
    charShoot: function ()
    {
        _character.sprite.animations.play("shoot", 20);
        if (_character.guns[_character.weaponIndex].currentBullets !== 0) {
            _character.guns[_character.weaponIndex].shootLightAnim(_character.posX, _character.posY, "ammoFlash");
        }
       
        _character.sprite.events.onAnimationComplete.addOnce(this.charShootComplete, this);
    },
    charReload: function ()
    {
        _character.sprite.animations.play("reload", 20);
        _character.sprite.events.onAnimationComplete.addOnce(this.charReloadedComplete, this);
    },
    charShootComplete: function ()
    {
        _character.currentGunState = _character.gunStates.NOTHING;
        //_character.guns[_character.weaponIndex].ammoFlashSprite.destroy();
    },
    charReloadedComplete: function () {
        _character.currentGunState = _character.gunStates.NOTHING;
        _character.guns[_character.weaponIndex].currentBullets = _character.guns[_character.weaponIndex].bullets;
    },
    charMeleeComplete: function() {
        _character.currentGunState = _character.gunStates.NOTHING;
    }

}
var _animManager = new AnimationManager();