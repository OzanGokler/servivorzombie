﻿var CollisionHandler = function () { };

CollisionHandler.prototype = {

    preload: function() {
        
    },

    create: function() {
        
    },

    update: function ()
    {

        //Collide zombies0 and character and hit to zombie
        if (_zombie0.zombies0 !== null)
        {
            for (var i = 0; i < _zombie0.zombies0.length; i++)
            {
                if (_zombie0.zombies0[i].sprite.body !== null)
                {
                    //Collide zombies0 and Character
                    game.physics.arcade.overlap(_character.placeHolder, _zombie0.zombies0[i].sprite, this.Character_and_Zombie0, null, this);

                    //collide bullets and zombie0
                    if (_character.guns[_character.weaponIndex].name !== "xm1014")
                    {
                        game.physics.arcade.overlap(_character.guns[_character.weaponIndex].bulletGroup, _zombie0.zombies0[i].sprite, this.Bullet_and_zombie0, null, this);
                    } else
                    {
                        game.physics.arcade.overlap(_character.guns[_character.weaponIndex].bulletGroup, _zombie0.zombies0[i].sprite, this.Bullet_and_zombie0, null, this);
                        game.physics.arcade.overlap(_character.guns[_character.weaponIndex].bulletGroup1, _zombie0.zombies0[i].sprite, this.Bullet_and_zombie0, null, this);
                        game.physics.arcade.overlap(_character.guns[_character.weaponIndex].bulletGroup2, _zombie0.zombies0[i].sprite, this.Bullet_and_zombie0, null, this);
                    }

                }
            }
        }

        //Collide zombies1 and character and hit to zombie
        if (_zombie1.zombies1 !== null)
        {
            for (var p = 0; p < _zombie1.zombies1.length; p++)
            {
                try {
                    if (_zombie1.zombies1[p].sprite.body !== null)
                    {
                        //Collide zombies1 and Character
                        game.physics.arcade.overlap(_character.placeHolder, _zombie1.zombies1[p].sprite, this.Character_and_Zombie1, null, this);

                        //collide bullets and zombie1
                        if (_character.guns[_character.weaponIndex].name !== "xm1014")
                        {
                            game.physics.arcade.overlap(_character.guns[_character.weaponIndex].bulletGroup, _zombie1.zombies1[p].sprite, this.Bullet_and_zombie1, null, this);
                        } else
                        {
                            game.physics.arcade.overlap(_character.guns[_character.weaponIndex].bulletGroup, _zombie1.zombies1[p].sprite, this.Bullet_and_zombie1, null, this);
                            game.physics.arcade.overlap(_character.guns[_character.weaponIndex].bulletGroup1, _zombie1.zombies1[p].sprite, this.Bullet_and_zombie1, null, this);
                            game.physics.arcade.overlap(_character.guns[_character.weaponIndex].bulletGroup2, _zombie1.zombies1[p].sprite, this.Bullet_and_zombie1, null, this);
                        }

                    }
                } catch (e) {

                } 

            }
        }

        //collide itemHealth and character
        if (_items.arr_itemHealth !== null) {
            for (var k = 0; k < _items.arr_itemHealth.length; k++)
            {
                game.physics.arcade.overlap(_character.placeHolder, _items.arr_itemHealth[k], this.itemHeal_and_Character, null, this);
                
            }
        }
        //collide itemFiveSeven and character
        if (_items.arr_itemFiveSeven !== null)
        {
            for (var l = 0; l < _items.arr_itemFiveSeven.length; l++)
            {
                game.physics.arcade.overlap(_character.placeHolder, _items.arr_itemFiveSeven[l], this.itemFiveSeven_and_Character, null, this);
            }
        }
        //collide itemAk47 and character
        if (_items.arr_itemAk47 !== null)
        {
            for (var m = 0; m < _items.arr_itemAk47.length; m++)
            {
                game.physics.arcade.overlap(_character.placeHolder, _items.arr_itemAk47[m], this.itemAk47_and_Character, null, this);
            }
        }
        //collide itemXm1014 and character
        if (_items.arr_itemXm1014 !== null)
        {
            for (var n = 0; n < _items.arr_itemXm1014.length; n++)
            {
                game.physics.arcade.overlap(_character.placeHolder, _items.arr_itemXm1014[n], this.itemXm1014_and_Character, null, this);
            }
        }
        //collide key and character
        if (_items.arr_itemKey !== null)
        {
            for (var o = 0; o < _items.arr_itemKey.length; o++)
            {
                game.physics.arcade.overlap(_character.placeHolder, _items.arr_itemKey[o], this.itemKey_and_Character, null, this);
            }
        };
        //collide door key and character
        if (_level.door !== null)
        {       
            game.physics.arcade.overlap(_character.placeHolder, _level.door, this.leveldoor_and_Character, null, this);           
        }
        if (_levelBoss.door !== null)
        {
            game.physics.arcade.overlap(_character.placeHolder, _levelBoss.door, this.levelBossDoor_and_character, null, this);
        }
        if (_level1.door !== null)
        {
            game.physics.arcade.overlap(_character.placeHolder, _level1.door, this.level1Door_and_character, null, this);
        }
        if (_levelBoss1.door !== null)
        {
            game.physics.arcade.overlap(_character.placeHolder, _levelBoss1.door, this.levelBoss1Door_and_character, null, this);
        }
        if (_level2.door !== null) {
            game.physics.arcade.overlap(_character.placeHolder, _level2.door, this.level2Door_and_character, null, this);
        }
        if (_levelBoss2.door !== null)
        {
            game.physics.arcade.overlap(_character.placeHolder, _levelBoss2.door, this.levelBoss2Door_and_character, null, this);
        }

        //collide bullet and boss, char and boss
        if (_boss0.sprite !== null) {

            //collide bullets
            try
            {
                if (_character.guns[_character.weaponIndex].name !== "xm1014")
                {
                    game.physics.arcade.overlap(_character.guns[_character.weaponIndex].bulletGroup, _boss0.sprite, this.bullet_and_boos0, null, this);
                } else
                {
                    game.physics.arcade.overlap(_character.guns[_character.weaponIndex].bulletGroup, _boss0.sprite, this.bullet_and_boos0, null, this);
                    game.physics.arcade.overlap(_character.guns[_character.weaponIndex].bulletGroup1, _boss0.sprite, this.bullet_and_boos0, null, this);
                    game.physics.arcade.overlap(_character.guns[_character.weaponIndex].bulletGroup2, _boss0.sprite, this.bullet_and_boos0, null, this);
                }
                
            } catch (e) {

            } 
            

            //collide char and boss0
            game.physics.arcade.overlap(_character.placeHolder, _boss0.sprite, this.char_and_boos0, null, this);
        }
        //Boss0 bullets collide
        if (_boss0.bulletGroup !== null) {
            game.physics.arcade.overlap(_character.placeHolder, _boss0.bulletGroup, this.char_and_flame, null, this);
        }

        //collide bullet and boss1, char and boss1
        if (_boss1.sprite !== null)
        {

            //collide bullets
            try
            {
                if (_character.guns[_character.weaponIndex].name !== "xm1014")
                {
                    game.physics.arcade.overlap(_character.guns[_character.weaponIndex].bulletGroup, _boss1.sprite, this.bullet_and_boos1, null, this);
                } else
                {
                    game.physics.arcade.overlap(_character.guns[_character.weaponIndex].bulletGroup, _boss1.sprite, this.bullet_and_boos1, null, this);
                    game.physics.arcade.overlap(_character.guns[_character.weaponIndex].bulletGroup1, _boss1.sprite, this.bullet_and_boos1, null, this);
                    game.physics.arcade.overlap(_character.guns[_character.weaponIndex].bulletGroup2, _boss1.sprite, this.bullet_and_boos1, null, this);
                }

            } catch (e)
            {

            }
            //collide char and boss1
            game.physics.arcade.overlap(_character.placeHolder, _boss1.sprite, this.char_and_boos1, null, this);
        }

        if (_boss1.bulletGroup !== null)
        {
            game.physics.arcade.overlap(_character.placeHolder, _boss1.bulletGroup, this.char_and_flame, null, this);
        }

        //collide bullet and boss2, char and boss1
        if (_boss2.sprite !== null)
        {

            //collide bullets
            try
            {
                if (_character.guns[_character.weaponIndex].name !== "xm1014")
                {
                    game.physics.arcade.overlap(_character.guns[_character.weaponIndex].bulletGroup, _boss2.sprite, this.bullet_and_boos2, null, this);
                } else
                {
                    game.physics.arcade.overlap(_character.guns[_character.weaponIndex].bulletGroup, _boss2.sprite, this.bullet_and_boos2, null, this);
                    game.physics.arcade.overlap(_character.guns[_character.weaponIndex].bulletGroup1, _boss2.sprite, this.bullet_and_boos2, null, this);
                    game.physics.arcade.overlap(_character.guns[_character.weaponIndex].bulletGroup2, _boss2.sprite, this.bullet_and_boos2, null, this);
                }

            } catch (e)
            {

            }
            //collide char and boss2
            game.physics.arcade.overlap(_character.placeHolder, _boss2.sprite, this.char_and_boos2, null, this);
        }

        if (_boss2.bulletGroup !== null)
        {
            game.physics.arcade.overlap(_character.placeHolder, _boss2.bulletGroup, this.char_and_flame, null, this);
            game.physics.arcade.overlap(_character.placeHolder, _boss2.bulletGroup2, this.char_and_flame, null, this);
            game.physics.arcade.overlap(_character.placeHolder, _boss2.bulletGroup3, this.char_and_flame, null, this);
            game.physics.arcade.overlap(_character.placeHolder, _boss2.bulletGroup4, this.char_and_flame, null, this);
            game.physics.arcade.overlap(_character.placeHolder, _boss2.bulletGroup5, this.char_and_flame, null, this);
            game.physics.arcade.overlap(_character.placeHolder, _boss2.bulletGroup6, this.char_and_flame, null, this);
            game.physics.arcade.overlap(_character.placeHolder, _boss2.bulletGroup7, this.char_and_flame, null, this);
            game.physics.arcade.overlap(_character.placeHolder, _boss2.bulletGroup8, this.char_and_flame, null, this);
        }
    },

    Bullet_and_zombie0: function (zombie0, bullet)
    {
        //when a bullet hits an zomibe0 we kill them both
        bullet.kill();
        _zombie0.zombies0[zombie0.name].sprite.animations.play("hit",15).onComplete.add(function () { _zombie0.zombies0[zombie0.name].sprite.animations.play("walk") });
        _zombie0.zombies0[zombie0.name].health -= _character.guns[_character.weaponIndex].damage;
        console.log(zombie0.name);
        if (_zombie0.zombies0[zombie0.name].health <= 0)
        {
            
            _zombie0.zombies0[zombie0.name].alive = false;
            var blood = game.add.sprite(_zombie0.zombies0[zombie0.name].sprite.x, _zombie0.zombies0[zombie0.name].sprite.y, "blood");
            blood.angle = _zombie0.zombies0[zombie0.name].sprite.angle;
            blood.anchor.setTo(0.5);
            //blood.scale.setTo(0.5);
            blood.animations.add("die", [0, 1, 2, 3, 4, 5]);
            blood.animations.play("die", 12);
            //zombisound arraydan vurulan zombi indexindeki sesi destroy ediyorum.
            _soundManager.zombieSoundArray[zombie0.name].destroy();
            _zombie0.zombies0[zombie0.name].sprite.animations.play("die").onComplete.add(function () { _zombie0.zombies0[zombie0.name].sprite.body = null; });
            game.add.tween(_zombie0.zombies0[zombie0.name].sprite).to({ alpha: 0 }, 2000, Phaser.Easing.Linear.None, true).onComplete.add(function ()
                 {
                    game.add.tween(blood).to({ alpha: 0 }, 2000, Phaser.Easing.Linear.None, true).onComplete.add(function (){ blood.destroy() });
                    _zombie0.zombies0[zombie0.name].sprite.destroy();
                 });
            if (_zombie0.zombies0[zombie0.name].itemHealt === true) {
                _items.itemHealth = game.add.sprite(_zombie0.zombies0[zombie0.name].sprite.x, _zombie0.zombies0[zombie0.name].sprite.y, "healthItem");
                //_items.itemHealth.scale.setTo(0.5);
                game.physics.enable(_items.itemHealth, Phaser.Physics.ARCADE);
                _items.arr_itemHealth.push(_items.itemHealth);
            }
            if (_zombie0.zombies0[zombie0.name].itemFiveSeven === true)
            {
                _items.itemFiveSeven = game.add.sprite(_zombie0.zombies0[zombie0.name].sprite.x, _zombie0.zombies0[zombie0.name].sprite.y, "fiveSevenItem");
                //_items.itemFiveSeven.scale.setTo(0.5);
                game.physics.enable(_items.itemFiveSeven, Phaser.Physics.ARCADE);
                _items.arr_itemFiveSeven.push(_items.itemFiveSeven);
            }
            if (_zombie0.zombies0[zombie0.name].itemAk47 === true)
            {
                _items.itemAk47 = game.add.sprite(_zombie0.zombies0[zombie0.name].sprite.x, _zombie0.zombies0[zombie0.name].sprite.y, "ak47Item");
                //_items.itemAk47.scale.setTo(0.5);
                game.physics.enable(_items.itemAk47, Phaser.Physics.ARCADE);
                _items.arr_itemAk47.push(_items.itemAk47);
            }
            if (_zombie0.zombies0[zombie0.name].itemXm1014 === true)
            {
                _items.itemXm1014 = game.add.sprite(_zombie0.zombies0[zombie0.name].sprite.x, _zombie0.zombies0[zombie0.name].sprite.y, "xm1014Item");
                //_items.itemXm1014.scale.setTo(0.5);
                game.physics.enable(_items.itemXm1014, Phaser.Physics.ARCADE);
                _items.arr_itemXm1014.push(_items.itemXm1014);
            }
            if (_zombie0.zombies0[zombie0.name].itemKey === true)
            {
                _items.itemKey = game.add.sprite(_zombie0.zombies0[zombie0.name].sprite.x, _zombie0.zombies0[zombie0.name].sprite.y, "keyItem");
                //_items.itemKey.scale.setTo(0.5);
                game.physics.enable(_items.itemKey, Phaser.Physics.ARCADE);
                _items.arr_itemKey.push(_items.itemKey);
            }

        }
    },

    Bullet_and_zombie1: function (zombie1, bullet)
    {
        //when a bullet hits an zomibe1 we kill them both
        bullet.kill();
        //_zombie0.zombies1[zombie1.name].sprite.animations.play("hit", 15).onComplete.add(function () { _zombie0.zombies1[zombie1.name].sprite.animations.play("walk") });
        _zombie1.zombies1[zombie1.name].health -= _character.guns[_character.weaponIndex].damage;
        if (_zombie1.zombies1[zombie1.name].health <= 150) {
            _zombie1.zombies1[zombie1.name].speed += 20;
        }
        if (_zombie1.zombies1[zombie1.name].health <= 100) {
            _zombie1.zombies1[zombie1.name].speed += 20;
        }
        if (_zombie1.zombies1[zombie1.name].health <= 50) {
            _zombie1.zombies1[zombie1.name].speed += 20;
        }
        if (_zombie1.zombies1[zombie1.name].health <= 0)
        {
            _zombie1.zombies1[zombie1.name].sprite.animations.stop(null, true);
            var blood = game.add.sprite(_zombie1.zombies1[zombie1.name].sprite.x, _zombie1.zombies1[zombie1.name].sprite.y, "blood");
            blood.angle = _zombie1.zombies1[zombie1.name].sprite.angle;
            blood.anchor.setTo(0.5);
            //blood.scale.setTo(0.5);
            blood.animations.add("die", [0, 1, 2, 3, 4, 5]);
            blood.animations.play("die", 12);
            _zombie1.zombies1[zombie1.name].alive = false;         
            _zombie1.zombies1[zombie1.name].sprite.body = null;
            //zombisound arraydan vurulan zombi indexindeki sesi destroy ediyorum.
            //_soundManager.zombieSoundArray[zombie1.name].destroy();
            //_zombie1.zombies1[zombie1.name].sprite.animations.play("die").onComplete.add(function () { _zombie0.zombies1[zombie1.name].sprite.body = null; });;
            game.add.tween(_zombie1.zombies1[zombie1.name].sprite).to({ alpha: 0 }, 2000, Phaser.Easing.Linear.None, true).onComplete.
                 add(function () {
                     game.add.tween(blood).to({ alpha: 0 }, 2000, Phaser.Easing.Linear.None, true).onComplete.add(function () { blood.destroy() });
    
                     _zombie1.zombies1[zombie1.name].sprite.destroy();
                 });
            if (_zombie1.zombies1[zombie1.name].itemHealt === true)
            {
                _items.itemHealth = game.add.sprite(_zombie1.zombies1[zombie1.name].sprite.x, _zombie1.zombies1[zombie1.name].sprite.y, "healthItem");
                //_items.itemHealth.scale.setTo(0.5);
                game.physics.enable(_items.itemHealth, Phaser.Physics.ARCADE);
                _items.arr_itemHealth.push(_items.itemHealth);
            }
            if (_zombie1.zombies1[zombie1.name].itemFiveSeven === true)
            {
                _items.itemFiveSeven = game.add.sprite(_zombie1.zombies1[zombie1.name].sprite.x, _zombie1.zombies1[zombie1.name].sprite.y, "fiveSevenItem");
                //_items.itemFiveSeven.scale.setTo(0.5);
                game.physics.enable(_items.itemFiveSeven, Phaser.Physics.ARCADE);
                _items.arr_itemFiveSeven.push(_items.itemFiveSeven);
            }
            if (_zombie1.zombies1[zombie1.name].itemAk47 === true)
            {
                _items.itemAk47 = game.add.sprite(_zombie1.zombies1[zombie1.name].sprite.x, _zombie1.zombies1[zombie1.name].sprite.y, "ak47Item");
                //_items.itemAk47.scale.setTo(0.5);
                game.physics.enable(_items.itemAk47, Phaser.Physics.ARCADE);
                _items.arr_itemAk47.push(_items.itemAk47);
            }
            if (_zombie1.zombies1[zombie1.name].itemXm1014 === true)
            {
                _items.itemXm1014 = game.add.sprite(_zombie1.zombies1[zombie1.name].sprite.x, _zombie1.zombies1[zombie1.name].sprite.y, "xm1014Item");
                //_items.itemXm1014.scale.setTo(0.5);
                game.physics.enable(_items.itemXm1014, Phaser.Physics.ARCADE);
                _items.arr_itemXm1014.push(_items.itemXm1014);
            }
            if (_zombie1.zombies1[zombie1.name].itemKey === true)
            {
                _items.itemKey = game.add.sprite(_zombie1.zombies1[zombie1.name].sprite.x, _zombie1.zombies1[zombie1.name].sprite.y, "keyItem");
                //_items.itemKey.scale.setTo(0.5);
                game.physics.enable(_items.itemKey, Phaser.Physics.ARCADE);
                _items.arr_itemKey.push(_items.itemKey);
            }

        }
    },


    Character_and_Zombie0: function (placeholder, z0) {
        if (_zombie0.currentBiteTime + _zombie0.biteTime < game.time.totalElapsedSeconds())
        {
                 
            if (_character.alive === true)
            {
                var rnd = Math.floor((Math.random() * 3));
                _character.health -= _zombie0.damage;
                _soundManager.charSoundArray[rnd].play();
                game.camera.shake(0.01, 300);
                console.log("ısır");
            }          
            if (_character.health <= 0 && _character.boolDie === false)
            {
                _soundManager.charDie();
                _character.alive = false;
                console.log("öl");
                _character.boolDie = true;
                _character.currentCharState = _character.charStates.DIE;
                game.camera.fade(0x000000, 2000);
                game.camera.onFadeComplete.add(function ()
                {

                    game.state.start(gameStates.GameOver);
                }, this);
            }
            _zombie0.currentBiteTime = game.time.totalElapsedSeconds();
            
        }
    },

    Character_and_Zombie1: function (placeholder, z0) {
        
        _zombie1.zombies1[z0.name].sprite.animations.play("z1attack", 60, true);
        if (_zombie1.currentBiteTime + _zombie1.biteTime < game.time.totalElapsedSeconds())
        {
            if (_character.alive === true)
            {
                var rnd = Math.floor((Math.random() * 3));
                _character.health -= _zombie1.damage;
                _soundManager.charSoundArray[rnd].play();
                game.camera.shake(0.01, 300);
                console.log("ısır");
            }
            if (_character.health <= 0 && _character.boolDie === false)
            {
                _soundManager.charDie();
                _character.alive = false;
                console.log("öl");
                _character.boolDie = true;
                _character.currentCharState = _character.charStates.DIE;
                game.camera.fade(0x000000, 2000);
                game.camera.onFadeComplete.add(function ()
                {

                    game.state.start(gameStates.GameOver);
                }, this);
            }
            _zombie1.currentBiteTime = game.time.totalElapsedSeconds();
        }
    },

    itemHeal_and_Character: function (char, itemhealth)
    {
        itemhealth.kill();
        if (_character.health <= 100)
        {
            _character.health += 20;
            if (_character.health > 100) {
                _character.health = 100;
            }
        }      
    },

    itemFiveSeven_and_Character: function (char, itemFiveSeven) {
        itemFiveSeven.kill();
        _character.guns[0].currentTotalBullets += 15;
    },

    itemAk47_and_Character: function (char, itemAk47)
    {
        itemAk47.kill();
        _character.guns[1].currentTotalBullets += 30;
    },

    itemXm1014_and_Character: function (char, itemXm1014)
    {
        itemXm1014.kill();
        _character.guns[2].currentTotalBullets += 8;
    },

    itemKey_and_Character: function (char, key)
    {
        key.kill();
        _character.ItemKey = true;
        console.log(_character.ItemKey);
    },

    leveldoor_and_Character: function (char, door)
    {

        if (_controller.buttonB_isDown === true && _character.boolDoorOnes === false)
        {
            _character.boolDoorOnes = true;
            if (_level.doorIsOpen === false)
            {
                if (_character.ItemKey === true)
                {
                    //acılıyor
                    _soundManager.openDoor();
                    door.animations.play("doorOpen", 4).onComplete.add(function () { _level.doorIsOpen = true; console.log("kapı acık " + _level.doorIsOpen) }, this);
                } else
                {
                    console.log("anahtarın yok ");
                    _soundManager.lockDoor();
                }

            } else
            {
                console.log("bosa geldin ");
                game.camera.fade(0x000000, 2000);
                game.camera.onFadeComplete.add(function() {
                    _soundManager.stopAllZombiesSound();
                    game.state.start(gameStates.GameManager1);
                }, this);
                
            }
        }
        if (_controller.buttonB_isDown === false)
        {
            _character.boolDoorOnes = false;
        }
    },

    levelBossDoor_and_character: function (char, door)
    {
        if (_controller.buttonB_isDown === true && _character.boolDoorOnes === false) {
            _character.boolDoorOnes = true;
            if (_levelBoss.doorIsOpen === false) {
                if (_character.ItemKey === true) {
                    //acılıyor
                    _soundManager.openDoor();
                    door.animations.play("doorOpen", 4).onComplete.add(function() {
                        _levelBoss.doorIsOpen = true;
                        console.log("kapı acık " + _levelBoss.doorIsOpen);
                    }, this);
                } else {
                    console.log("anahtarın yok ");
                    _soundManager.lockDoor();
                }

            } else
            {
                game.camera.fade(0x000000, 2000);
                game.camera.onFadeComplete.add(function ()
                {
                    _soundManager.bossMusicSound.stop();
                    game.state.start(gameStates.GameManager2);
                }, this);
                console.log("level1 hos geldin");
            }
        }
        if (_controller.buttonB_isDown === false)
        {
            _character.boolDoorOnes = false;
        }
    },

    level1Door_and_character: function (char, door)
    {
        if (_controller.buttonB_isDown === true && _character.boolDoorOnes === false)
        {
            _character.boolDoorOnes = true;
            if (_level1.doorIsOpen === false)
            {
                if (_character.ItemKey === true)
                {
                    //acılıyor
                    _soundManager.openDoor();
                    door.animations.play("doorOpen", 4).onComplete.add(function ()
                    {
                        _level1.doorIsOpen = true;
                        console.log("kapı acık " + _level1.doorIsOpen);
                    }, this);
                } else
                {
                    console.log("anahtarın yok ");
                    _soundManager.lockDoor();
                }

            } else
            {
                game.camera.fade(0x000000, 2000);
                game.camera.onFadeComplete.add(function () {
                    _soundManager.stopAllZombies1Sound();
                    game.state.start(gameStates.GameManager3);
                }, this);
            }
        }
        if (_controller.buttonB_isDown === false)
        {
            _character.boolDoorOnes = false;
        }
    },

    levelBoss1Door_and_character: function (char, door)
    {
        if (_controller.buttonB_isDown === true && _character.boolDoorOnes === false)
        {
            _character.boolDoorOnes = true;
            if (_levelBoss1.doorIsOpen === false)
            {
                if (_character.ItemKey === true)
                {
                    //acılıyor
                    _soundManager.openDoor();
                    door.animations.play("doorOpen", 4).onComplete.add(function ()
                    {
                        _levelBoss1.doorIsOpen = true;
                        console.log("kapı acık " + _levelBoss1.doorIsOpen);
                    }, this);
                } else
                {
                    _soundManager.lockDoor();
                }

            } else
            {
                game.camera.fade(0x000000, 2000);
                game.camera.onFadeComplete.add(function () {
                    _soundManager.boss1MusicSound.stop();
                    game.state.start(gameStates.GameManager4);
                }, this);
                
                //game.state.add(gameStates.GameManager4, _gameManager4);
                //game.state.start(gameStates.GameManager4);
            }
        }
        if (_controller.buttonB_isDown === false)
        {
            _character.boolDoorOnes = false;
        }
    },

    levelBoss2Door_and_character: function (char, door)
    {
        if (_controller.buttonB_isDown === true && _character.boolDoorOnes === false)
        {
            _character.boolDoorOnes = true;
            if (_levelBoss2.doorIsOpen === false)
            {
                if (_character.ItemKey === true)
                {
                    //acılıyor
                    _soundManager.openDoor();
                    door.animations.play("doorOpen", 4).onComplete.add(function ()
                    {
                        _levelBoss2.doorIsOpen = true;
                        console.log("kapı acık " + _levelBoss2.doorIsOpen);
                    }, this);
                } else
                {
                    _soundManager.lockDoor();
                }

            } else
            {
                game.camera.fade(0x000000, 2000);
                game.camera.onFadeComplete.add(function () {
                    _soundManager.boss2MusicSound.stop();
                    game.state.start(gameStates.GameFinish);
                }, this);

                //game.state.add(gameStates.GameManager4, _gameManager4);
                //game.state.start(gameStates.GameManager4);
            }
        }
        if (_controller.buttonB_isDown === false)
        {
            _character.boolDoorOnes = false;
        }
    },

    level2Door_and_character: function (char, door)
    {
        if (_controller.buttonB_isDown === true && _character.boolDoorOnes === false) {
            _character.boolDoorOnes = true;
            if (_level2.doorIsOpen === false)
            {
                if (_character.ItemKey === true)
                {
                    //acılıyor
                    _soundManager.openDoor();
                    door.animations.play("doorOpen", 4).onComplete.add(function ()
                    {
                        _level2.doorIsOpen = true;
                        console.log("kapı acık " + _level2.doorIsOpen);
                    }, this);
                } else
                {
                    _soundManager.lockDoor();
                }

            } else
            {
                game.camera.fade(0x000000, 2000);
                game.camera.onFadeComplete.add(function ()
                {
                    _soundManager.stopAllZombies1Sound();
                    _soundManager.stopAllZombiesSound();
                    game.state.start(gameStates.GameManager5);
                }, this);

                //game.state.add(gameStates.GameManager4, _gameManager4);
                //game.state.start(gameStates.GameManager4);
            }
        }
        if (_controller.buttonB_isDown === false)
        {
            _character.boolDoorOnes = false;
        }
    },

    bullet_and_boos0: function (boss0, bullet) {
        bullet.kill();
        _boss0.health -= _character.guns[_character.weaponIndex].damage;
        if (_boss0.health <= 0) {
            _boss0.alive = false;
            var blood = game.add.sprite(_boss0.sprite.x, _boss0.sprite.y, "blood");
            blood.angle = _boss0.sprite.angle;
            blood.anchor.setTo(0.5);
            blood.scale.setTo(2);
            blood.animations.add("die", [0, 1, 2, 3, 4, 5]);
            blood.animations.play("die", 12);
            if (_boss0.key === true) {
                _items.itemKey = game.add.sprite(_boss0.sprite.x, _boss0.sprite.y, "keyItem");
                //_items.itemKey.scale.setTo(0.5);
                game.physics.enable(_items.itemKey, Phaser.Physics.ARCADE);
                _items.arr_itemKey.push(_items.itemKey);
            }
            boss0.destroy();
        }
    },
    char_and_boos0: function (char, boss0)
    {
        console.log(boss0.sprite);

        if (_boss0.currentBiteTime + _boss0.reTouchrate < game.time.totalElapsedSeconds()) {
            if (_character.alive === true)
            {
                var rnd = Math.floor((Math.random() * 3));
                _character.health -= _boss0.damage;
                _soundManager.charSoundArray[rnd].play();
                game.camera.shake(0.01, 300);
                console.log("ısır");
            }
            if (_character.health <= 0 && _character.boolDie === false)
            {
                _soundManager.charDie();
                _character.alive = false;
                console.log("öl");
                _character.boolDie = true;
                _character.currentCharState = _character.charStates.DIE;
                game.camera.fade(0x000000, 2000);
                game.camera.onFadeComplete.add(function ()
                {

                    game.state.start(gameStates.GameOver);
                }, this);
            }
            _boss0.currentBiteTime = game.time.totalElapsedSeconds();
            
        }
    },
    char_and_flame: function (char, flame) {
        flame.kill();
        console.log(char.name);

        if (_character.alive === true)
        {
            var rnd = Math.floor((Math.random() * 3));
            _character.health -= 20;
            _soundManager.charSoundArray[rnd].play();
            game.camera.shake(0.01, 300);
            console.log("ısır");
        }
        if (_character.health <= 0 && _character.boolDie === false)
        {
            _soundManager.charDie();
            _character.alive = false;
            console.log("öl");
            _character.boolDie = true;
            _character.currentCharState = _character.charStates.DIE;
            game.camera.fade(0x000000, 2000);
            game.camera.onFadeComplete.add(function ()
            {

                game.state.start(gameStates.GameOver);
            }, this);
        }
    },
    bullet_and_boos1: function (boss1, bullet)
    {
        bullet.kill();
        _boss1.health -= _character.guns[_character.weaponIndex].damage;
        if (_boss1.health <= 0)
        {
            _boss1.alive = false;
            var blood = game.add.sprite(_boss1.sprite.x, _boss1.sprite.y, "blood");
            blood.angle = _boss0.sprite.angle;
            blood.anchor.setTo(0.5);
            blood.scale.setTo(2);
            blood.animations.add("die", [0, 1, 2, 3, 4, 5]);
            blood.animations.play("die", 12);
            if (_boss1.key === true)
            {
                _items.itemKey = game.add.sprite(_boss1.sprite.x, _boss1.sprite.y, "keyItem");
                //_items.itemKey.scale.setTo(0.5);
                game.physics.enable(_items.itemKey, Phaser.Physics.ARCADE);
                _items.arr_itemKey.push(_items.itemKey);
            }
            boss1.destroy();
        }
    },

    bullet_and_boos2: function (boss2, bullet)
    {
        bullet.kill();
        _boss2.health -= _character.guns[_character.weaponIndex].damage;
        if (_boss2.health <= 0)
        {
            _boss2.alive = false;
            var blood = game.add.sprite(_boss2.sprite.x, _boss2.sprite.y, "blood");
            blood.angle = _boss0.sprite.angle;
            blood.anchor.setTo(0.5);
            blood.scale.setTo(2);
            blood.animations.add("die", [0, 1, 2, 3, 4, 5]);
            blood.animations.play("die", 12);
            if (_boss2.key === true)
            {
                _items.itemKey = game.add.sprite(_boss2.sprite.x, _boss2.sprite.y, "keyItem");
                //_items.itemKey.scale.setTo(0.5);
                game.physics.enable(_items.itemKey, Phaser.Physics.ARCADE);
                _items.arr_itemKey.push(_items.itemKey);
            }
            boss2.destroy();
        }
    },
    char_and_boos1: function (char, boss1)
    {

        if (_boss1.currentBiteTime + _boss1.reTouchrate < game.time.totalElapsedSeconds())
        {
            if (_character.alive === true)
            {
                var rnd = Math.floor((Math.random() * 3));
                _character.health -= _boss1.damage;
                _soundManager.charSoundArray[rnd].play();
                game.camera.shake(0.01, 300);
                console.log("ısır");
            }
            if (_character.health <= 0 && _character.boolDie === false)
            {
                _soundManager.charDie();
                _character.alive = false;
                console.log("öl");
                _character.boolDie = true;
                _character.currentCharState = _character.charStates.DIE;
                game.camera.fade(0x000000, 2000);
                game.camera.onFadeComplete.add(function ()
                {

                    game.state.start(gameStates.GameOver);
                }, this);
            }
            _boss1.currentBiteTime = game.time.totalElapsedSeconds();

        }
    },
    char_and_boos2: function (char, boss2)
    {

        if (_boss2.currentBiteTime + _boss2.reTouchrate < game.time.totalElapsedSeconds())
        {
            if (_character.alive === true)
            {
                var rnd = Math.floor((Math.random() * 3));
                _character.health -= _boss2.damage;
                _soundManager.charSoundArray[rnd].play();
                game.camera.shake(0.01, 300);
                console.log("ısır");
            }
            if (_character.health <= 0 && _character.boolDie === false)
            {
                _soundManager.charDie();
                _character.alive = false;
                console.log("öl");
                _character.boolDie = true;
                _character.currentCharState = _character.charStates.DIE;
                game.camera.fade(0x000000, 2000);
                game.camera.onFadeComplete.add(function ()
                {

                    game.state.start(gameStates.GameOver);
                }, this);
            }
            _boss2.currentBiteTime = game.time.totalElapsedSeconds();

        }
    },

}
var _collisionHandler = new CollisionHandler();