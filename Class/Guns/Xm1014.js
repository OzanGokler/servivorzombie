﻿var Xm1014 = function () { };

Xm1014.prototype = {
    name: "xm1014",
    damage: 60,
    magazine: 100,
    bullets: 8,
    currentBullets: null,
    totalBullets: 24,
    currentTotalBullets: null,
    bulletSprite: null,
    refirerate: 500,
    sprite: null,
    boolcreateBulletsOnes: false,
    bulletSpeed: 1300,
    bulletGroup: null,
    bulletGroup1: null,
    bulletGroup2: null,

    preload: function () {

    },

    create: function () {
        this.sprite = game.add.sprite(_character.posX, _character.posY, "xm1014");
        if (_character.sprite !== null) {
            this.sprite.angle = _character.sprite.angle;
        }
        this.sprite.anchor.setTo(0.5);
        this.sprite.scale.setTo(2);
        this.sprite.animations.add("idle", [2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19]);
        this.sprite.animations.add("melee", [22, 23, 24, 25, 26, 27, 28, 29, 30, 31, 32, 33, 34]);
        this.sprite.animations.add("walk", [37, 38, 39, 40, 41, 42, 43, 44, 45, 46, 47, 48, 49, 50, 51, 52, 53, 54]);
        this.sprite.animations.add("reload", [57, 58, 59, 60, 61, 62, 63, 64, 65, 66, 67, 68, 69, 70, 71, 72, , 73, 74]);
        this.sprite.animations.add("shoot", [75, 76, 77]);
        //create first bullets count
        if (this.boolcreateBulletsOnes === false)
        {

            //create bullet group
            this.bulletGroup = game.add.group();
            this.bulletGroup.enableBody = true;
            this.bulletGroup.physicsBodyType = Phaser.Physics.ARCADE;
            this.bulletGroup.createMultiple(this.bullets, "xm1014Bullet");
            this.bulletGroup.setAll("anchor.x", 0.5);
            //this.bulletGroup.setAll("scale.x", 0.5);
            //this.bulletGroup.setAll("scale.y", 0.5);
            this.bulletGroup.setAll("outOfBoundsKill", true);
            this.bulletGroup.setAll("checkWorldBounds", true);

            //create bullet1 group
            this.bulletGroup1 = game.add.group();
            this.bulletGroup1.enableBody = true;
            this.bulletGroup1.physicsBodyType = Phaser.Physics.ARCADE;
            this.bulletGroup1.createMultiple(this.bullets, "xm1014Bullet");
            this.bulletGroup1.setAll("anchor.x", 0.5);
            //this.bulletGroup1.setAll("scale.x", 0.5);
            //this.bulletGroup1.setAll("scale.y", 0.5);
            this.bulletGroup1.setAll("outOfBoundsKill", true);
            this.bulletGroup1.setAll("checkWorldBounds", true);

            //create bullet2 group
            this.bulletGroup2 = game.add.group();
            this.bulletGroup2.enableBody = true;
            this.bulletGroup2.physicsBodyType = Phaser.Physics.ARCADE;
            this.bulletGroup2.createMultiple(this.bullets, "xm1014Bullet");
            this.bulletGroup2.setAll("anchor.x", 0.5);
            //this.bulletGroup2.setAll("scale.x", 0.5);
            //this.bulletGroup2.setAll("scale.y", 0.5);
            this.bulletGroup2.setAll("outOfBoundsKill", true);
            this.bulletGroup2.setAll("checkWorldBounds", true);

            this.currentTotalBullets = this.bullets * this.magazine;
            this.currentBullets = this.bullets;
            this.boolcreateBulletsOnes = true;
        }
    },

    update: function ()
    {

    },

    createBullet: function (direction, x, y)
    {
        var currentBullet = this.bulletGroup.getFirstExists(false);
        var currentBullet1 = this.bulletGroup1.getFirstExists(false);
        var currentBullet2 = this.bulletGroup2.getFirstExists(false);

        if (direction === _character.directions.RIGHT)
        {
            currentBullet.reset(x, y);
            currentBullet.body.velocity.x = this.bulletSpeed;

            currentBullet1.reset(x, y);
            currentBullet1.body.velocity.y = 500;
            currentBullet1.body.velocity.x = 1200;

            currentBullet2.reset(x, y);
            currentBullet2.body.velocity.y = -500;
            currentBullet2.body.velocity.x = 1200;
        }
        if (direction === _character.directions.UP)
        {
            currentBullet.reset(x, y);
            currentBullet.body.velocity.y = -this.bulletSpeed;

            currentBullet1.reset(x, y);
            currentBullet1.body.velocity.y = -1200;
            currentBullet1.body.velocity.x = 500;

            currentBullet2.reset(x, y);
            currentBullet2.body.velocity.y = -1200;
            currentBullet2.body.velocity.x = -500;
        }
        if (direction === _character.directions.LEFT)
        {
            currentBullet.reset(x, y);
            currentBullet.body.velocity.x = -this.bulletSpeed;

            currentBullet1.reset(x, y);
            currentBullet1.body.velocity.y = -500;
            currentBullet1.body.velocity.x = -1200;

            currentBullet2.reset(x, y);
            currentBullet2.body.velocity.y = 500;
            currentBullet2.body.velocity.x = -1200;
        }
        if (direction === _character.directions.DOWN)
        {
            currentBullet.reset(x, y);
            currentBullet.body.velocity.y = +this.bulletSpeed;

            currentBullet1.reset(x, y);
            currentBullet1.body.velocity.y = 1200;
            currentBullet1.body.velocity.x = 500;

            currentBullet2.reset(x, y);
            currentBullet2.body.velocity.y = 1200;
            currentBullet2.body.velocity.x = -500;
        }
        if (direction === _character.directions.UPRIGHT)
        {
            currentBullet.reset(x, y);
            currentBullet.body.velocity.y = -this.bulletSpeed / Math.sqrt(2);
            currentBullet.body.velocity.x = +this.bulletSpeed / Math.sqrt(2);

            currentBullet1.reset(x, y);
            currentBullet1.body.velocity.y = -1200;
            currentBullet1.body.velocity.x = 500;

            currentBullet2.reset(x, y);
            currentBullet2.body.velocity.y = -500;
            currentBullet2.body.velocity.x = 1200;

        }
        if (direction === _character.directions.UPLEFT)
        {
            currentBullet.reset(x, y);
            currentBullet.body.velocity.y = -this.bulletSpeed / Math.sqrt(2);
            currentBullet.body.velocity.x = -this.bulletSpeed / Math.sqrt(2);

            currentBullet1.reset(x, y);
            currentBullet1.body.velocity.y = -1200;
            currentBullet1.body.velocity.x = -500;

            currentBullet2.reset(x, y);
            currentBullet2.body.velocity.y = -500;
            currentBullet2.body.velocity.x = -1200;
        }
        if (direction === _character.directions.DOWNLEFT)
        {
            currentBullet.reset(x, y);
            currentBullet.body.velocity.y = +this.bulletSpeed / Math.sqrt(2);
            currentBullet.body.velocity.x = -this.bulletSpeed / Math.sqrt(2);

            currentBullet1.reset(x, y);
            currentBullet1.body.velocity.y = 1200;
            currentBullet1.body.velocity.x = -500;

            currentBullet2.reset(x, y);
            currentBullet2.body.velocity.y = 500;
            currentBullet2.body.velocity.x = -1200;
        }
        if (direction === _character.directions.DOWNRIGHT)
        {
            currentBullet.reset(x, y);
            currentBullet.body.velocity.y = +this.bulletSpeed / Math.sqrt(2);
            currentBullet.body.velocity.x = +this.bulletSpeed / Math.sqrt(2);

            currentBullet1.reset(x, y);
            currentBullet1.body.velocity.y = 500;
            currentBullet1.body.velocity.x = 1200;

            currentBullet2.reset(x, y);
            currentBullet2.body.velocity.y = 1200;
            currentBullet2.body.velocity.x = 500;
        }
    },

     fireBullet: function ()
    {
        if (this.currentTotalBullets !== 0)
        {           
            if (this.currentBullets !== 0)
            {
                this.currentBullets--;
                this.currentTotalBullets--;
                game.camera.shake(0.007, 300);
                _soundManager.shotGunShot();
                _soundManager.shotGunShot2();
            }
            else
            {
                _character.currentGunState = _character.gunStates.RELOAD;
                _animManager.charReload();
                _soundManager.shotGunReload();
                this.magazine--;
            }
        }
        if (this.currentTotalBullets === 0)
        {
            _character.currentGunState = _character.gunStates.MELEE;
            _animManager.charMelee();
        }

    },

     shootLightAnim: function (x, y)
     {
         if (_character.currentDirection === _character.directions.RIGHT)
         {
             this.ammoFlashSprite = game.add.sprite(x + 75, y + 25, "ammoFlash");
             this.createBullet(_character.currentDirection, x + 75, y + 25);
         }
         if (_character.currentDirection === _character.directions.UP)
         {
             this.ammoFlashSprite = game.add.sprite(x + 25, y - 75, "ammoFlash");
             this.ammoFlashSprite.angle = -90;
             this.createBullet(_character.currentDirection, x + 25, y - 75);
         }
         if (_character.currentDirection === _character.directions.LEFT)
         {
             this.ammoFlashSprite = game.add.sprite(x - 75, y - 25, "ammoFlash");
             this.ammoFlashSprite.angle = -180;
             this.createBullet(_character.currentDirection, x - 75, y - 25);
         }
         if (_character.currentDirection === _character.directions.DOWN)
         {
             this.ammoFlashSprite = game.add.sprite(x - 25, y + 75, "ammoFlash");
             this.ammoFlashSprite.angle = -270;
             this.createBullet(_character.currentDirection, x - 25, y + 75);
         }
         if (_character.currentDirection === _character.directions.UPRIGHT)
         {
             this.ammoFlashSprite = game.add.sprite(x + 73, y - 36, "ammoFlash");
             this.ammoFlashSprite.angle = -45;
             this.createBullet(_character.currentDirection, x + 73, y - 36);
         }
         if (_character.currentDirection === _character.directions.UPLEFT)
         {
             this.ammoFlashSprite = game.add.sprite(x - 36, y - 73, "ammoFlash");
             this.ammoFlashSprite.angle = -135;
             this.createBullet(_character.currentDirection, x - 36, y - 73);
         }
         if (_character.currentDirection === _character.directions.DOWNLEFT)
         {
             this.ammoFlashSprite = game.add.sprite(x - 73, y + 36, "ammoFlash");
             this.ammoFlashSprite.angle = -225;
             this.createBullet(_character.currentDirection, x - 73, y + 36);
         }
         if (_character.currentDirection === _character.directions.DOWNRIGHT)
         {
             this.ammoFlashSprite = game.add.sprite(x + 36, y + 73, "ammoFlash");
             this.ammoFlashSprite.angle = +45;
             this.createBullet(_character.currentDirection, x + 36, y + 73);
         }

         this.ammoFlashSprite.anchor.setTo(0.5);
         this.ammoFlashSprite.scale.setTo(0.14);

     }
}
