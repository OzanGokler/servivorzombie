﻿var Ak47 = function () { };

Ak47.prototype = {
    name: "ak47",
    damage: 35,
    magazine: 3,
    bullets: 30,
    totalBullets: null,
    bulletSprite: null,
    refirerate: 200,
    sprite: null,
    boolcreateBulletsOnes: false,
    bulletSpeed: 1300,
    bulletGroup: null,

    preload: function () {
        
    },

    create: function ()
    {
        //GUN SPRITE CREATE
        this.sprite = game.add.sprite(_character.posX, _character.posY, "ak47");
        if (_character.sprite !== null) {
            this.sprite.angle = _character.sprite.angle;
        }
        this.sprite.anchor.setTo(0.5);
        this.sprite.scale.setTo(2);
        this.sprite.animations.add("idle", [2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19]);
        this.sprite.animations.add("melee", [22, 23, 24, 25, 26, 27, 28, 29, 30, 31, 32, 33, 34]);
        this.sprite.animations.add("walk", [37, 38, 39, 40, 41, 42, 43, 44, 45, 46, 47, 48, 49, 50, 51, 52, 53, 54]);
        this.sprite.animations.add("reload", [57, 58, 59, 60, 61, 62, 63, 64, 65, 66, 67, 68, 69, 70, 71, 72, , 73, 74]);
        this.sprite.animations.add("shoot", [75, 76, 77]);
        //create first bullets count
        if (this.boolcreateBulletsOnes === false)
        {
            //create bullet group
            this.bulletGroup = game.add.group();
            this.bulletGroup.enableBody = true;
            this.bulletGroup.physicsBodyType = Phaser.Physics.ARCADE;
            this.bulletGroup.createMultiple(this.bullets, "ak47Bullet");
            this.bulletGroup.setAll("anchor.x", 0.5);
            //this.bulletGroup.setAll("scale.x", 0.5);
            //this.bulletGroup.setAll("scale.y", 0.5);
            this.bulletGroup.setAll("outOfBoundsKill", true);
            this.bulletGroup.setAll("checkWorldBounds", true);

            this.currentTotalBullets = this.bullets * this.magazine;
            this.currentBullets = this.bullets;
            this.boolcreateBulletsOnes = true;
        }

    },
    
    update: function() {
        
    },

    createBullet: function (direction, x, y)
    {
        var currentBullet = this.bulletGroup.getFirstExists(false);

        if (direction === _character.directions.RIGHT)
        {
            currentBullet.reset(x, y);
            currentBullet.body.velocity.x = this.bulletSpeed;
        }
        if (direction === _character.directions.UP)
        {
            currentBullet.reset(x, y);
            currentBullet.body.velocity.y = -this.bulletSpeed;
        }
        if (direction === _character.directions.LEFT)
        {
            currentBullet.reset(x, y);
            currentBullet.body.velocity.x = -this.bulletSpeed;
        }
        if (direction === _character.directions.DOWN)
        {
            currentBullet.reset(x, y);
            currentBullet.body.velocity.y = +this.bulletSpeed;
        }
        if (direction === _character.directions.UPRIGHT)
        {
            currentBullet.reset(x, y);
            currentBullet.body.velocity.y = -this.bulletSpeed / Math.sqrt(2);
            currentBullet.body.velocity.x = +this.bulletSpeed / Math.sqrt(2);
        }
        if (direction === _character.directions.UPLEFT)
        {
            currentBullet.reset(x, y);
            currentBullet.body.velocity.y = -this.bulletSpeed / Math.sqrt(2);
            currentBullet.body.velocity.x = -this.bulletSpeed / Math.sqrt(2);
        }
        if (direction === _character.directions.DOWNLEFT)
        {
            currentBullet.reset(x, y);
            currentBullet.body.velocity.y = +this.bulletSpeed / Math.sqrt(2);
            currentBullet.body.velocity.x = -this.bulletSpeed / Math.sqrt(2);
        }
        if (direction === _character.directions.DOWNRIGHT)
        {
            currentBullet.reset(x, y);
            currentBullet.body.velocity.y = +this.bulletSpeed / Math.sqrt(2);
            currentBullet.body.velocity.x = +this.bulletSpeed / Math.sqrt(2);
        }
    },

    fireBullet: function ()
    {
        if (this.currentTotalBullets !== 0)
        {
            if (this.currentBullets !== 0)
            {
                this.currentBullets--;
                this.currentTotalBullets--;
                game.camera.shake(0.005, 200);
                _soundManager.ak47Shot();
            }
            else
            {
                _character.currentGunState = _character.gunStates.RELOAD;
                _animManager.charReload();
                _soundManager.ak47Reload();
                this.magazine--;
            }
        }
        if (this.currentTotalBullets === 0)
        {
            _character.currentGunState = _character.gunStates.MELEE;
            _animManager.charMelee();
        }

    },
    shootLightAnim: function (x, y)
    {
        if (_character.currentDirection === _character.directions.RIGHT)
        {
            this.ammoFlashSprite = game.add.sprite(x + 75, y + 25, "ammoFlash");
            this.createBullet(_character.currentDirection, x + 75, y + 25);
        }
        if (_character.currentDirection === _character.directions.UP)
        {
            this.ammoFlashSprite = game.add.sprite(x + 25, y - 75, "ammoFlash");
            this.ammoFlashSprite.angle = -90;
            this.createBullet(_character.currentDirection, x + 25, y - 75);
        }
        if (_character.currentDirection === _character.directions.LEFT)
        {
            this.ammoFlashSprite = game.add.sprite(x - 75, y - 25, "ammoFlash");
            this.ammoFlashSprite.angle = -180;
            this.createBullet(_character.currentDirection, x - 75, y - 25);
        }
        if (_character.currentDirection === _character.directions.DOWN)
        {
            this.ammoFlashSprite = game.add.sprite(x - 25, y + 75, "ammoFlash");
            this.ammoFlashSprite.angle = -270;
            this.createBullet(_character.currentDirection, x - 25, y + 75);
        }
        if (_character.currentDirection === _character.directions.UPRIGHT)
        {
            this.ammoFlashSprite = game.add.sprite(x + 73, y - 36, "ammoFlash");
            this.ammoFlashSprite.angle = -45;
            this.createBullet(_character.currentDirection, x + 73, y - 36);
        }
        if (_character.currentDirection === _character.directions.UPLEFT)
        {
            this.ammoFlashSprite = game.add.sprite(x - 36, y - 73, "ammoFlash");
            this.ammoFlashSprite.angle = -135;
            this.createBullet(_character.currentDirection, x - 36, y - 73);
        }
        if (_character.currentDirection === _character.directions.DOWNLEFT)
        {
            this.ammoFlashSprite = game.add.sprite(x - 73, y + 36, "ammoFlash");
            this.ammoFlashSprite.angle = -225;
            this.createBullet(_character.currentDirection, x - 73, y + 36);
        }
        if (_character.currentDirection === _character.directions.DOWNRIGHT)
        {
            this.ammoFlashSprite = game.add.sprite(x + 36, y + 73, "ammoFlash");
            this.ammoFlashSprite.angle = +45;
            this.createBullet(_character.currentDirection, x + 36, y + 73);
        }

        this.ammoFlashSprite.anchor.setTo(0.5);
        this.ammoFlashSprite.scale.setTo(0.12);

    }
}
