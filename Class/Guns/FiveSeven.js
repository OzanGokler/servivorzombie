﻿var FiveSeven = function () { };

FiveSeven.prototype = {
    name: "fiveseven",
    damage: 20,
    magazine: 3,
    bullets: 15,
    currentBullets: null,
    totalBullets: 45,
    currentTotalBullets: null,
    bulletSprite: null,
    refirerate: 300,
    sprite: null,
    ammoFlashSprite: null,
    boolcreateBulletsOnes: false,
    bulletSpeed: 1300,
    bulletGroup: null,

    preload: function () {
            
    },

    create: function () {
        
        this.sprite = game.add.sprite(_character.posX, _character.posY, "fiveseven");
        if (_character.sprite !== null)
        {
            this.sprite.angle = _character.sprite.angle;
        }       
        this.sprite.anchor.setTo(0.5);
        this.sprite.scale.setTo(2);
        this.sprite.animations.add("idle", [2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19]);
        this.sprite.animations.add("melee", [22, 23, 24, 25, 26, 27, 28, 29, 30, 31, 32, 33, 34]);
        this.sprite.animations.add("walk", [37, 38, 39, 40, 41, 42, 43, 44, 45, 46, 47, 48, 49, 50, 51, 52, 53, 54]);
        this.sprite.animations.add("reload", [56, 57, 58, 59, 60, 61, 62, 63, 64, 65, 66, 67, 68, 69]);
        this.sprite.animations.add("shoot", [70, 71, 72]);

        //create first bullets count
        if (this.boolcreateBulletsOnes === false)
        {
            //create bullet group
            this.bulletGroup = game.add.group();
            this.bulletGroup.enableBody = true;
            this.bulletGroup.physicsBodyType = Phaser.Physics.ARCADE;
            this.bulletGroup.createMultiple(this.bullets, "fiveSevenBullet");
            this.bulletGroup.setAll("anchor.x", 0.5);
            //this.bulletGroup.setAll("scale.x", 0.5);
            //this.bulletGroup.setAll("scale.y", 0.5);
            this.bulletGroup.setAll("outOfBoundsKill", true);
            this.bulletGroup.setAll("checkWorldBounds", true); 

            this.currentTotalBullets = this.bullets * this.magazine;
            this.currentBullets = this.bullets;
            this.boolcreateBulletsOnes = true;
        }
       
    },

    update: function ()
    {

    },



    createBullet: function(direction, x, y) {
        var currentBullet = this.bulletGroup.getFirstExists(false);

        if (direction === _character.directions.RIGHT) {
            currentBullet.reset(x, y);
            currentBullet.body.velocity.x = this.bulletSpeed;
        }
        if (direction === _character.directions.UP)
        {
            currentBullet.reset(x, y);
            currentBullet.body.velocity.y = -this.bulletSpeed;
        }
        if (direction === _character.directions.LEFT)
        {
            currentBullet.reset(x, y);
            currentBullet.body.velocity.x = -this.bulletSpeed;
        }
        if (direction === _character.directions.DOWN)
        {
            currentBullet.reset(x, y);
            currentBullet.body.velocity.y = +this.bulletSpeed;
        }
        if (direction === _character.directions.UPRIGHT) 
        {
            currentBullet.reset(x, y);
            currentBullet.body.velocity.y = -this.bulletSpeed / Math.sqrt(2);
            currentBullet.body.velocity.x = +this.bulletSpeed / Math.sqrt(2);
        }
        if (direction === _character.directions.UPLEFT)
        {
            currentBullet.reset(x, y);
            currentBullet.body.velocity.y = -this.bulletSpeed / Math.sqrt(2);
            currentBullet.body.velocity.x = -this.bulletSpeed / Math.sqrt(2);
        }
        if (direction === _character.directions.DOWNLEFT)
        {
            currentBullet.reset(x, y);
            currentBullet.body.velocity.y = +this.bulletSpeed / Math.sqrt(2);
            currentBullet.body.velocity.x = -this.bulletSpeed / Math.sqrt(2);
        }
        if (direction === _character.directions.DOWNRIGHT)
        {
            currentBullet.reset(x, y);
            currentBullet.body.velocity.y = +this.bulletSpeed / Math.sqrt(2);
            currentBullet.body.velocity.x = +this.bulletSpeed / Math.sqrt(2);
        }
    },

    fireBullet: function ()
    {
        if (this.currentTotalBullets !== 0)
        {           
            if (this.currentBullets !== 0)
            {
                this.currentBullets--;
                this.currentTotalBullets--;
                //this.cameraTween();
                game.camera.shake(0.0025, 200);
                _soundManager.pistolShot();
            }
            else
            {
                _character.currentGunState = _character.gunStates.RELOAD;
                _animManager.charReload();
                _soundManager.pistolReload();
                this.magazine--;
            }
        }
        if (this.currentTotalBullets === 0)
        {
            _character.currentGunState = _character.gunStates.MELEE;
            _animManager.charMelee();
        }
        

    },

    shootLightAnim: function (x, y)
    {
        if (_character.currentDirection === _character.directions.RIGHT)
        {
            this.ammoFlashSprite = game.add.sprite(x + 68, y + 28, "ammoFlash");
            this.createBullet(_character.currentDirection, x + 68, y + 28);
        }
        if (_character.currentDirection === _character.directions.UP)
        {
            this.ammoFlashSprite = game.add.sprite(x + 28, y - 68, "ammoFlash");
            this.ammoFlashSprite.angle = -90;
            this.createBullet(_character.currentDirection, x + 28, y - 68);
        }
        if (_character.currentDirection === _character.directions.LEFT)
        {
            this.ammoFlashSprite = game.add.sprite(x - 68, y - 28, "ammoFlash");
            this.ammoFlashSprite.angle = -180;
            this.createBullet(_character.currentDirection, x - 68, y - 28);
        }
        if (_character.currentDirection === _character.directions.DOWN)
        {
            this.ammoFlashSprite = game.add.sprite(x - 28, y + 68, "ammoFlash");
            this.ammoFlashSprite.angle = -270;
            this.createBullet(_character.currentDirection, x - 28, y + 68);
        }
        if (_character.currentDirection === _character.directions.UPRIGHT)
        {
            this.ammoFlashSprite = game.add.sprite(x + 68, y - 28, "ammoFlash");
            this.ammoFlashSprite.angle = -45;
            this.createBullet(_character.currentDirection, x + 68, y - 28);
        }
        if (_character.currentDirection === _character.directions.UPLEFT)
        {
            this.ammoFlashSprite = game.add.sprite(x - 28, y - 68, "ammoFlash");
            this.ammoFlashSprite.angle = -135;
            this.createBullet(_character.currentDirection, x - 28, y - 68);
        }
        if (_character.currentDirection === _character.directions.DOWNLEFT)
        {
            this.ammoFlashSprite = game.add.sprite(x - 68, y + 28, "ammoFlash");
            this.ammoFlashSprite.angle = -225;
            this.createBullet(_character.currentDirection, x - 68, y + 28);
        }
        if (_character.currentDirection === _character.directions.DOWNRIGHT)
        {
            this.ammoFlashSprite = game.add.sprite(x + 28, y + 68, "ammoFlash");
            this.ammoFlashSprite.angle = +45;
            this.createBullet(_character.currentDirection, x + 28, y + 68);
        }

        this.ammoFlashSprite.anchor.setTo(0.5);
        this.ammoFlashSprite.scale.setTo(0.1);

    },

    //cameraTween: function() {
    //    if (_character.currentDirection === _character.directions.RIGHT) {
    //        var tween = game.add.tween(game.camera).to({ x: game.camera.position.x - 5 }, 12, Phaser.Easing.Bounce.Out, true);

    //        console.log("bang");
    //        tween.yoyo(true, 1);
    //    }
    //}
}