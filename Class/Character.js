﻿var Character = function() {
};

Character.prototype = {
    posX: null,
    posY: null,
    placeHolder: null,
    health: 100,
    name: null,
    sprite: null,
    boolChangeWeaponOnes: false,
    boolDoorOnes: false,
    boolshootOnes: false,
    weaponIndex: 0,
    charStates: { IDLE: 0, WALK: 1, DIE: 2 },
    currentCharState: null,
    gunStates: { NOTHING: 0, SHOOT: 1, RELOAD: 2, MELEE: 3 },
    currentGunState: null,
    directions: { RIGHT: 0, DOWNRIGHT: 1, DOWN: 2, DOWNLEFT: 3, LEFT: 4, UPLEFT: 5, UP: 6, UPRIGHT: 7 },
    currentDirection: null,
    guns: null,
    charSpeed: 4,
    gunFlashTimer: null,
    shotgunShootTimer: null,
    charColision: null,
    ItemKey: false,
    currentLevel: null,
    footSoundCount: null,
    footSoundDistance: 25,
    boolReload: false,
    boolDie: false,
    alive:null,

    preload: function () {
        
    },
    create: function () {
        this.alive = true;
        this.boolDie = false;
        this.boolStart = false;
        this.itemKey = false;
        this.posX = 100;
        this.posY = 100;
        this.placeHolder = game.add.sprite(this.posX, this.posY, "placeHolder");
        this.placeHolder.scale.setTo(2);
        this.placeHolder.alpha = 0;
        _soundManager.charSoundArray = new Array();
        _soundManager.charSoundCreate();
        
        this.name = "OGB";
        this.health = 100;
        this.guns = new Array();
        var fiveSeven = new FiveSeven();
        var ak47 = new Ak47();
        var xm1014 = new Xm1014();            
        this.guns.push(fiveSeven);
        this.guns.push(ak47);
        this.guns.push(xm1014);
        this.changeWeapon(this.weaponIndex);
        this.sprite.angle = 0;       
        game.physics.enable(this.placeHolder, Phaser.Physics.ARCADE);
        this.placeHolder.anchor.setTo(0.5);
        this.currentCharState = this.charStates.IDLE;
        this.currentDirection = this.directions.RIGHT;
        this.currentGunState = this.gunStates.NOTHING;
        this.sprite.animations.play("idle", 20);

        //this.startButton.events.BUTTON_9.onInputDown.add()
        //game.physics.enable(this.sprite, Phaser.Physics.ARCADE);
        //this.sprite.body.immovable = true;


    },
    update: function ()
    {
        if (this.currentCharState !== this.charStates.DIE) {
            this.Movement();
        }
        
        game.world.bringToTop(this.sprite);
        game.camera.follow(this.sprite,Phaser.Camera.FOLLOW_LOCKON, 0.1, 0.1);
    },

    CharDirection: function ()
    {
        if (this.currentDirection === this.directions.RIGHT)
        {
            this.sprite.angle = 0;
        }
        if (this.currentDirection === this.directions.LEFT)
        {
            this.sprite.angle = -180;
        }
        if (this.currentDirection === this.directions.UP)
        {
            this.sprite.angle = -90;
        }
        if (this.currentDirection === this.directions.DOWN)
        {
            this.sprite.angle = -270;
        }
        if (this.currentDirection === this.directions.DOWNRIGHT)
        {
            this.sprite.angle = +45;
        }
        if (this.currentDirection === this.directions.DOWNLEFT)
        {
            this.sprite.angle = +135;
        }
        if (this.currentDirection === this.directions.UPRIGHT)
        {
            this.sprite.angle = -45;
        }
        if (this.currentDirection === this.directions.UPLEFT)
        {
            this.sprite.angle = -135;
        }

    },

    changeWeapon: function (index) {

        switch (index) {
            case 0:
                if (this.sprite !== null) {
                    this.sprite.destroy();
                }       
                this.guns[index].create();
                this.sprite = this.guns[index].sprite;
                break;
            case 1:
                if (this.sprite !== null)
                {
                    this.sprite.destroy();
                }
                this.guns[index].create();
                this.sprite = this.guns[index].sprite;
                break;
            case 2:
                if (this.sprite !== null)
                {
                    this.sprite.destroy();
                }
                this.guns[index].create();
                this.sprite = this.guns[index].sprite;
                break;
        }       
    },

    openDoor:function() {
        
    },

    shoot: function ()
    {
        //Shoot
        if (this.guns[this.weaponIndex].name === "fiveseven")
        {
            if (_controller.buttonX_isDown === true &&
                this.boolshootOnes === false &&
                this.currentGunState !== this.gunStates.RELOAD &&
                this.currentGunState !== this.gunStates.SHOOT)
            {
                this.currentGunState = this.gunStates.SHOOT;
                _animManager.charShoot();
                this.gunFlashTimer = game.time.totalElapsedSeconds();
                this.guns[this.weaponIndex].fireBullet();
                this.boolshootOnes = true;
            }
            if (_controller.buttonX_isDown === false)
            {
                this.boolshootOnes = false;
            }
        }
        else if (this.guns[this.weaponIndex].name === "xm1014")
        {
            if (this.shotgunShootTimer + 1 < game.time.totalElapsedSeconds()) {
                if (_controller.buttonX_isDown === true &&
               this.boolshootOnes === false &&
               this.currentGunState !== this.gunStates.RELOAD &&
               this.currentGunState !== this.gunStates.SHOOT)
                {
                    this.currentGunState = this.gunStates.SHOOT;
                    _animManager.charShoot();
                    this.shotgunShootTimer = game.time.totalElapsedSeconds();
                    this.gunFlashTimer = game.time.totalElapsedSeconds();
                    this.guns[this.weaponIndex].fireBullet();
                    this.boolshootOnes = true;
                }
                if (_controller.buttonX_isDown === false)
                {
                    this.boolshootOnes = false;
                }
            }         
        }
        else
        {
            if (_controller.buttonX_isDown === true && this.currentGunState !== this.gunStates.RELOAD && this.currentGunState !== this.gunStates.SHOOT)
            {
                this.currentGunState = this.gunStates.SHOOT;
                _animManager.charShoot();
                this.gunFlashTimer = game.time.totalElapsedSeconds();
                this.guns[this.weaponIndex].fireBullet();
                
            }
        }
        //gun flash destroy
        if (this.gunFlashTimer + 0.05 < game.time.totalElapsedSeconds()) {
            if (this.guns[this.weaponIndex].ammoFlashSprite !== null && this.guns[this.weaponIndex].ammoFlashSprite !== undefined)
            {
                this.guns[this.weaponIndex].ammoFlashSprite.kill();
            }
           
        }

    },

    Movement: function () {
        this.shoot();
        if (_controller.buttonA_isDown === true) {
            this.currentCharState = this.charStates.WALK;
            this.footSoundPlay();
            
        } else
        {
            this.currentCharState = this.charStates.IDLE;
        }

        //MoveRigh
        this.CharDirection();   
        if (_controller.buttonRight_isDown === true) {
            this.currentDirection = this.directions.RIGHT;         
            this.CharDirection();
            if (this.currentCharState === this.charStates.WALK)
            {
                this.sprite.x += this.charSpeed;
                this.posX = this.sprite.x;
                this.placeHolder.x = this.posX;
            }                     
            if (this.currentGunState === this.gunStates.NOTHING)
            {
                _animManager.charWalk();
            }
        }
        //MoveLeft
        if (_controller.buttonLeft_isDown === true)
        {
            this.currentDirection = this.directions.LEFT;
            this.CharDirection();
            if (this.currentCharState === this.charStates.WALK)
            {
                this.sprite.x -= this.charSpeed;
                this.posX = this.sprite.x;
                this.placeHolder.x = this.posX;
            }
            
            if (this.currentGunState === this.gunStates.NOTHING)
            {
                _animManager.charWalk();
            }
        }
        //MoveUp
        if (_controller.buttonUp_isDown === true)
        {
            this.currentDirection = this.directions.UP;
            this.CharDirection();
            if (this.currentCharState === this.charStates.WALK)
            {
                this.sprite.y -= this.charSpeed;
                this.posY = this.sprite.y;
                this.placeHolder.y = this.posY;
            }
            
            if (this.currentGunState === this.gunStates.NOTHING)
            {
                _animManager.charWalk();
            }
        }
        //MoveDown
        if (_controller.buttonDown_isDown === true)
        {
            this.currentDirection = this.directions.DOWN;
            this.CharDirection();
            if (this.currentCharState === this.charStates.WALK)
            {
                this.sprite.y += this.charSpeed;
                this.posY = this.sprite.y;
                this.placeHolder.y = this.posY;
            }
            
            if (this.currentGunState === this.gunStates.NOTHING)
            {
                _animManager.charWalk();
            }
        }
            //MoveDownRight
        if (_controller.buttonDown_isDown === true && _controller.buttonRight_isDown)
        {
            this.currentDirection = this.directions.DOWNRIGHT;
            this.CharDirection();
            if (this.currentGunState === this.gunStates.NOTHING)
            {
                _animManager.charWalk();
            }
        }
        //MoveDownLeft
        if (_controller.buttonDown_isDown === true && _controller.buttonLeft_isDown)
        {
            this.currentDirection = this.directions.DOWNLEFT;
            this.CharDirection();
            if (this.currentGunState === this.gunStates.NOTHING)
            {
                _animManager.charWalk();
            }
        }
        //MoveUpLeft
        if (_controller.buttonUp_isDown === true && _controller.buttonLeft_isDown)
        {
            this.currentDirection = this.directions.UPLEFT;
            this.CharDirection();
            if (this.currentGunState === this.gunStates.NOTHING)
            {
                _animManager.charWalk();
            }
        }
        //MoveUpRight
        if (_controller.buttonUp_isDown === true && _controller.buttonRight_isDown)
        {
            this.currentDirection = this.directions.UPRIGHT;
            this.CharDirection();
            if (this.currentGunState === this.gunStates.NOTHING)
            {
                _animManager.charWalk();
            }
        }
        //ChangeWeapon
        if (_controller.buttonY_isDown === true && this.boolChangeWeaponOnes === false)
        {
            this.weaponIndex++;
            if (this.weaponIndex >= this.guns.length)
            {
                this.weaponIndex = 0;
            }
            this.currentGunState = this.gunStates.NOTHING;
            this.changeWeapon(this.weaponIndex);
            _animManager.charIdle();
            this.boolChangeWeaponOnes = true;
        }
        if (_controller.buttonY_isDown === false) {
            this.boolChangeWeaponOnes = false;
        }        
       
        if (_controller.buttonUp_isDown === false &&
            _controller.buttonDown_isDown === false &&
            _controller.buttonRight_isDown === false &&
            _controller.buttonLeft_isDown === false)
        {

            if (this.currentGunState === this.gunStates.NOTHING) {
                _animManager.charIdle();
            }
                
        }

    },

    //FootSound
    footSoundPlay: function() {
        //footSounds
        if (this.currentDirection === this.directions.RIGHT &&
            this.currentDirection !== this.directions.LEFT &&
            this.currentDirection !== this.directions.UP &&
            this.currentDirection !== this.directions.DOWN &&
            this.currentDirection !== this.directions.UPRIGHT &&
            this.currentDirection !== this.directions.UPLEFT &&
            this.currentDirection !== this.directions.DOWNRIGHT &&
            this.currentDirection !== this.directions.DOWNLEFT)
        {
            this.footSoundCount++;
            if (this.footSoundCount % this.footSoundDistance === 0)
            {
                _soundManager.foot();
            }
        };
        if (this.currentDirection !== this.directions.RIGHT &&
            this.currentDirection === this.directions.LEFT &&
            this.currentDirection !== this.directions.UP &&
            this.currentDirection !== this.directions.DOWN &&
            this.currentDirection !== this.directions.UPRIGHT &&
            this.currentDirection !== this.directions.UPLEFT &&
            this.currentDirection !== this.directions.DOWNRIGHT &&
            this.currentDirection !== this.directions.DOWNLEFT)
        {
            this.footSoundCount++;
            if (this.footSoundCount % this.footSoundDistance === 0)
            {
                _soundManager.foot();
            }
        };
        if (this.currentDirection !== this.directions.RIGHT &&
            this.currentDirection !== this.directions.LEFT &&
            this.currentDirection === this.directions.UP &&
            this.currentDirection !== this.directions.DOWN &&
            this.currentDirection !== this.directions.UPRIGHT &&
            this.currentDirection !== this.directions.UPLEFT &&
            this.currentDirection !== this.directions.DOWNRIGHT &&
            this.currentDirection !== this.directions.DOWNLEFT)
        {
            this.footSoundCount++;
            if (this.footSoundCount % this.footSoundDistance === 0)
            {
                _soundManager.foot();
            }
        };
        if (this.currentDirection !== this.directions.RIGHT &&
            this.currentDirection !== this.directions.LEFT &&
            this.currentDirection !== this.directions.UP &&
            this.currentDirection === this.directions.DOWN &&
            this.currentDirection !== this.directions.UPRIGHT &&
            this.currentDirection !== this.directions.UPLEFT &&
            this.currentDirection !== this.directions.DOWNRIGHT &&
            this.currentDirection !== this.directions.DOWNLEFT)
        {
            this.footSoundCount++;
            if (this.footSoundCount % this.footSoundDistance === 0)
            {
                _soundManager.foot();
            }
        };
        if (this.currentDirection !== this.directions.RIGHT &&
            this.currentDirection !== this.directions.LEFT &&
            this.currentDirection !== this.directions.UP &&
            this.currentDirection !== this.directions.DOWN &&
            this.currentDirection === this.directions.UPRIGHT &&
            this.currentDirection !== this.directions.UPLEFT &&
            this.currentDirection !== this.directions.DOWNRIGHT &&
            this.currentDirection !== this.directions.DOWNLEFT)
        {
            this.footSoundCount++;
            if (this.footSoundCount % this.footSoundDistance === 0)
            {
                _soundManager.foot();
            }
        };
        if (this.currentDirection !== this.directions.RIGHT &&
            this.currentDirection !== this.directions.LEFT &&
            this.currentDirection !== this.directions.UP &&
            this.currentDirection !== this.directions.DOWN &&
            this.currentDirection !== this.directions.UPRIGHT &&
            this.currentDirection === this.directions.UPLEFT &&
            this.currentDirection !== this.directions.DOWNRIGHT &&
            this.currentDirection !== this.directions.DOWNLEFT)
        {
            this.footSoundCount++;
            if (this.footSoundCount % this.footSoundDistance === 0)
            {
                _soundManager.foot();
            }
        };
        if (this.currentDirection !== this.directions.RIGHT &&
            this.currentDirection !== this.directions.LEFT &&
            this.currentDirection !== this.directions.UP &&
            this.currentDirection !== this.directions.DOWN &&
            this.currentDirection !== this.directions.UPRIGHT &&
            this.currentDirection !== this.directions.UPLEFT &&
            this.currentDirection === this.directions.DOWNRIGHT &&
            this.currentDirection !== this.directions.DOWNLEFT)
        {
            this.footSoundCount++;
            if (this.footSoundCount % this.footSoundDistance === 0)
            {
                _soundManager.foot();
            }
        };
        if (this.currentDirection !== this.directions.RIGHT &&
            this.currentDirection !== this.directions.LEFT &&
            this.currentDirection !== this.directions.UP &&
            this.currentDirection !== this.directions.DOWN &&
            this.currentDirection !== this.directions.UPRIGHT &&
            this.currentDirection !== this.directions.UPLEFT &&
            this.currentDirection !== this.directions.DOWNRIGHT &&
            this.currentDirection === this.directions.DOWNLEFT)
        {
            this.footSoundCount++;
            if (this.footSoundCount % this.footSoundDistance === 0)
            {
                _soundManager.foot();
            }
        };
    }
}
var _character = new Character()