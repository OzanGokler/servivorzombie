﻿var Boss0 = function () { };

Boss0.prototype = {
    name: "nemesis",
    alive: null,
    health: null,
    speed: 300,
    sprite: null,
    refireRate: 1,
    flame: null,
    refirerate: 2,
    currentShot: 0,
    bulletGroup: null,
    bullets: 50,
    bulletSpeed: 300,
    currentBiteTime: 0,
    reTouchrate: 1,
    rotationStates: { UP: 0, DOWN: 1 },
    currentRotationState: null,
    key: null,
    damage : 40,
    
    preload: function ()
    {

    },

    create: function () {
        this.health = 250;
        this.sprite = game.add.sprite(550, 100, "boss0");
        this.currentRotationState = this.rotationStates.DOWN;
        this.alive = true;
        this.sprite.name = this.name;
        game.physics.enable(this.sprite, Phaser.Physics.ARCADE);      
        this.sprite.anchor.setTo(0.5);
        this.sprite.scale.setTo(0.7);
        //Bullet group created
        this.bulletGroup = game.add.group();
        this.bulletGroup.enableBody = true;
        this.bulletGroup.physicsBodyType = Phaser.Physics.ARCADE;
        this.bulletGroup.createMultiple(this.bullets, "flame");

        this.bulletGroup.callAll("animations.add", "animations", "fire", [0, 1, 2, 3, 4], 10, true);
        this.bulletGroup.callAll("play", null, "fire");
        this.bulletGroup.setAll("anchor.x", 0.5);
        this.bulletGroup.setAll("scale.x", -1);
        this.bulletGroup.setAll("scale.y", 1);
        this.bulletGroup.setAll("outOfBoundsKill", true);
        this.bulletGroup.setAll("checkWorldBounds", true);

        this.key = true;

        //this.flame = game.add.sprite(this.sprite.x - 30, this.sprite.y, "flame");
        //this.flame.anchor.setTo(0.5);
        //this.flame.scale.setTo(-1, 1);
        //this.flame.animations.add("fire", [0, 1, 2, 3, 4]);
        //this.flame.animations.play("fire", 5, true);

    },

    update: function ()
    {
        if (this.alive === true) {
            this.movement();
        }
    },

    shot: function() {
        if (this.currentShot + this.refireRate < game.time.totalElapsedSeconds()) {
            var currentBullet = this.bulletGroup.getFirstExists(false);
            currentBullet.reset(this.sprite.x-10, this.sprite.y-30);
            currentBullet.body.velocity.x = -this.bulletSpeed;
            this.currentShot = game.time.totalElapsedSeconds();
        }
        
    },

    movement: function() {
        if (this.currentRotationState === this.rotationStates.DOWN) {
            if (this.sprite.position.y < 400) {
                this.sprite.body.velocity.y = this.speed;
            } else {
                this.currentRotationState = this.rotationStates.UP;
            }
        }
        if (this.currentRotationState === this.rotationStates.UP)
        {
            if (this.sprite.position.y > 20) {
                this.sprite.body.velocity.y = -this.speed;
            } else {
                this.currentRotationState = this.rotationStates.DOWN;
            }
        }
        this.shot();
    }

}
var _boss0 = new Boss0();