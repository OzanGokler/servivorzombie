﻿var Boss2 = function () { };

Boss2.prototype = {
    name: "nemesis2",
    alive: null,
    health: null,
    speed: 75,
    sprite: null,
    refireRate: 2,
    flame: null,
    refirerate: 2,
    currentShot: 0,
    bulletGroup: null,
    bulletGroup2: null,
    bulletGroup3: null,
    bulletGroup4: null,
    bulletGroup5: null,
    bulletGroup6: null,
    bulletGroup7: null,
    bulletGroup8: null,
    bullets: 50,
    bulletSpeed: 200,
    currentBiteTime: 0,
    reTouchrate: 1,
    rotationStates: { UP: 0, DOWN: 1 },
    currentRotationState: null,
    key: null,
    bodyAttackTime: 0,
    bodyAttakTimePass: 4,
    damage: 40,
    
    preload: function ()
    {

    },

    create: function () {
        this.health = 250;
        this.sprite = game.add.sprite(550, 240, "boss2");
        this.currentRotationState = this.rotationStates.DOWN;
        this.alive = true;
        this.sprite.name = this.name;
        game.physics.enable(this.sprite, Phaser.Physics.ARCADE);      
        this.sprite.anchor.setTo(0.5);
        //this.sprite.scale.setTo(0.7);

        //Bullet group created
        this.bulletGroup = game.add.group();
        this.bulletGroup.enableBody = true;
        this.bulletGroup.physicsBodyType = Phaser.Physics.ARCADE;
        this.bulletGroup.createMultiple(this.bullets, "flame");
        this.bulletGroup.callAll("animations.add", "animations", "fire", [0, 1, 2, 3, 4], 10, true);
        this.bulletGroup.callAll("play", null, "fire");
        this.bulletGroup.setAll("anchor.x", 0.5);
        this.bulletGroup.setAll("scale.x", -1);
        this.bulletGroup.setAll("scale.y", 1);
        this.bulletGroup.setAll("outOfBoundsKill", true);
        this.bulletGroup.setAll("checkWorldBounds", true);

        this.bulletGroup2 = game.add.group();
        this.bulletGroup2.enableBody = true;
        this.bulletGroup2.physicsBodyType = Phaser.Physics.ARCADE;
        this.bulletGroup2.createMultiple(this.bullets, "flame");
        this.bulletGroup2.callAll("animations.add", "animations", "fire", [0, 1, 2, 3, 4], 10, true);
        this.bulletGroup2.callAll("play", null, "fire");
        this.bulletGroup2.setAll("anchor.x", 0.5);
        this.bulletGroup2.setAll("scale.x", -1);
        this.bulletGroup2.setAll("scale.y", 1);
        this.bulletGroup2.setAll("outOfBoundsKill", true);
        this.bulletGroup2.setAll("checkWorldBounds", true);

        this.bulletGroup3 = game.add.group();
        this.bulletGroup3.enableBody = true;
        this.bulletGroup3.physicsBodyType = Phaser.Physics.ARCADE;
        this.bulletGroup3.createMultiple(this.bullets, "flame");
        this.bulletGroup3.callAll("animations.add", "animations", "fire", [0, 1, 2, 3, 4], 10, true);
        this.bulletGroup3.callAll("play", null, "fire");
        this.bulletGroup3.setAll("anchor.x", 0.5);
        this.bulletGroup3.setAll("scale.x", -1);
        this.bulletGroup3.setAll("scale.y", 1);
        this.bulletGroup3.setAll("outOfBoundsKill", true);
        this.bulletGroup3.setAll("checkWorldBounds", true);

        this.bulletGroup4 = game.add.group();
        this.bulletGroup4.enableBody = true;
        this.bulletGroup4.physicsBodyType = Phaser.Physics.ARCADE;
        this.bulletGroup4.createMultiple(this.bullets, "flame");
        this.bulletGroup4.callAll("animations.add", "animations", "fire", [0, 1, 2, 3, 4], 10, true);
        this.bulletGroup4.callAll("play", null, "fire");
        this.bulletGroup4.setAll("anchor.x", 0.5);
        this.bulletGroup4.setAll("scale.x", -1);
        this.bulletGroup4.setAll("scale.y", 1);
        this.bulletGroup4.setAll("outOfBoundsKill", true);
        this.bulletGroup4.setAll("checkWorldBounds", true);

        this.bulletGroup5 = game.add.group();
        this.bulletGroup5.enableBody = true;
        this.bulletGroup5.physicsBodyType = Phaser.Physics.ARCADE;
        this.bulletGroup5.createMultiple(this.bullets, "flame");
        this.bulletGroup5.callAll("animations.add", "animations", "fire", [0, 1, 2, 3, 4], 10, true);
        this.bulletGroup5.callAll("play", null, "fire");
        this.bulletGroup5.setAll("anchor.x", 0.5);
        this.bulletGroup5.setAll("scale.x", -1);
        this.bulletGroup5.setAll("scale.y", 1);
        this.bulletGroup5.setAll("outOfBoundsKill", true);
        this.bulletGroup5.setAll("checkWorldBounds", true);

        this.bulletGroup6 = game.add.group();
        this.bulletGroup6.enableBody = true;
        this.bulletGroup6.physicsBodyType = Phaser.Physics.ARCADE;
        this.bulletGroup6.createMultiple(this.bullets, "flame");
        this.bulletGroup6.callAll("animations.add", "animations", "fire", [0, 1, 2, 3, 4], 10, true);
        this.bulletGroup6.callAll("play", null, "fire");
        this.bulletGroup6.setAll("anchor.x", 0.5);
        this.bulletGroup6.setAll("scale.x", -1);
        this.bulletGroup6.setAll("scale.y", 1);
        this.bulletGroup6.setAll("outOfBoundsKill", true);
        this.bulletGroup6.setAll("checkWorldBounds", true);

        this.bulletGroup7 = game.add.group();
        this.bulletGroup7.enableBody = true;
        this.bulletGroup7.physicsBodyType = Phaser.Physics.ARCADE;
        this.bulletGroup7.createMultiple(this.bullets, "flame");
        this.bulletGroup7.callAll("animations.add", "animations", "fire", [0, 1, 2, 3, 4], 10, true);
        this.bulletGroup7.callAll("play", null, "fire");
        this.bulletGroup7.setAll("anchor.x", 0.5);
        this.bulletGroup7.setAll("scale.x", -1);
        this.bulletGroup7.setAll("scale.y", 1);
        this.bulletGroup7.setAll("outOfBoundsKill", true);
        this.bulletGroup7.setAll("checkWorldBounds", true);

        this.bulletGroup8 = game.add.group();
        this.bulletGroup8.enableBody = true;
        this.bulletGroup8.physicsBodyType = Phaser.Physics.ARCADE;
        this.bulletGroup8.createMultiple(this.bullets, "flame");
        this.bulletGroup8.callAll("animations.add", "animations", "fire", [0, 1, 2, 3, 4], 10, true);
        this.bulletGroup8.callAll("play", null, "fire");
        this.bulletGroup8.setAll("anchor.x", 0.5);
        this.bulletGroup8.setAll("scale.x", -1);
        this.bulletGroup8.setAll("scale.y", 1);
        this.bulletGroup8.setAll("outOfBoundsKill", true);
        this.bulletGroup8.setAll("checkWorldBounds", true);

        this.key = true;

        this.bodyAttackTime = game.time.totalElapsedSeconds();

    },

    update: function ()
    {
        if (this.alive === true) {
            this.movement();
        }
    },

    shot: function() {
        if (this.currentShot + this.refireRate < game.time.totalElapsedSeconds())
        {

            var currentBullet = this.bulletGroup.getFirstExists(false);
            currentBullet.reset(this.sprite.x, this.sprite.y);
            currentBullet.body.velocity.x = -Math.sqrt(2)*this.bulletSpeed;

            var currentBullet2 = this.bulletGroup2.getFirstExists(false);
            currentBullet2.reset(this.sprite.x, this.sprite.y);
            currentBullet2.angle = 45;
            currentBullet2.body.velocity.x = -this.bulletSpeed;
            currentBullet2.body.velocity.y = -this.bulletSpeed;

            var currentBullet3 = this.bulletGroup3.getFirstExists(false);
            currentBullet3.reset(this.sprite.x, this.sprite.y);
            currentBullet3.angle = 90;
            currentBullet3.body.velocity.y = -Math.sqrt(2) * this.bulletSpeed;;

            var currentBullet4 = this.bulletGroup4.getFirstExists(false);
            currentBullet4.reset(this.sprite.x, this.sprite.y);
            currentBullet4.angle = 125;
            currentBullet4.body.velocity.x = this.bulletSpeed;
            currentBullet4.body.velocity.y = -this.bulletSpeed;

            var currentBullet5 = this.bulletGroup5.getFirstExists(false);
            currentBullet5.reset(this.sprite.x, this.sprite.y);
            currentBullet5.angle = 180;
            currentBullet5.body.velocity.x = Math.sqrt(2) * this.bulletSpeed;

            var currentBullet6 = this.bulletGroup6.getFirstExists(false);
            currentBullet6.reset(this.sprite.x , this.sprite.y);
            currentBullet6.angle = 225;
            currentBullet6.body.velocity.x = +this.bulletSpeed;
            currentBullet6.body.velocity.y = +this.bulletSpeed;

            var currentBullet7 = this.bulletGroup7.getFirstExists(false);
            currentBullet7.reset(this.sprite.x, this.sprite.y);
            currentBullet7.angle = 270;
            currentBullet7.body.velocity.y = Math.sqrt(2) * this.bulletSpeed;;

            var currentBullet8 = this.bulletGroup8.getFirstExists(false);
            currentBullet8.reset(this.sprite.x, this.sprite.y);
            currentBullet8.angle = 315;
            currentBullet8.body.velocity.x = -this.bulletSpeed;
            currentBullet8.body.velocity.y = this.bulletSpeed;


            this.currentShot = game.time.totalElapsedSeconds();
        }
        
    },

    movement: function ()
    {
        game.physics.arcade.moveToObject(this.sprite, _character.sprite, this.speed);
        if (this.bodyAttackTime + this.bodyAttakTimePass < game.time.totalElapsedSeconds())
        {
            var tween = game.add.tween(this.sprite).to({ x: _character.posX, y:_character.posY}, 1000, Phaser.Easing.Linear.Out, true);
            //tween.yoyo(true, 1);
            this.bodyAttackTime = game.time.totalElapsedSeconds();
        }
        


        //if (this.currentRotationState === this.rotationStates.DOWN) {
        //    if (this.sprite.position.y < 200) {
        //        this.sprite.body.velocity.y = this.speed;
        //    } else {
        //        this.currentRotationState = this.rotationStates.UP;
        //    }
        //}
        //if (this.currentRotationState === this.rotationStates.UP)
        //{
        //    if (this.sprite.position.y > 20) {
        //        this.sprite.body.velocity.y = -this.speed;
        //    } else {
        //        this.currentRotationState = this.rotationStates.DOWN;
        //    }
        //}
        this.shot();
    }

}
var _boss2 = new Boss2();