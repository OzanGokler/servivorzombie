﻿var Debug = function () { };

Debug.prototype = {
    look: function () {
        //game.debug.text(_character.guns[0].name + " " + _character.guns[1].name + " " + _character.guns[2].name, 10, 45);
        var gunText = _character.guns[_character.weaponIndex].name + " : " + _character.guns[_character.weaponIndex].currentTotalBullets + "/" + _character.guns[_character.weaponIndex].currentBullets;
        var animText = "CharState:" + _character.currentCharState + " " + "GunState:" + _character.currentGunState + " " + "DirectionState:" + _character.currentDirection;
        var health = "Health: " + _character.health;
        game.debug.text(gunText, 10, 15);
        //game.debug.text(animText, 10, 30);
        game.debug.text(health, 10, 30);
    }
}

var _debug = new Debug();