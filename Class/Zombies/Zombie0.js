﻿var Zombie0 = function () { };

Zombie0.prototype = {
    health: null,
    player: null,
    alive: null,
    zombieStates: { IDLE: 0, WALK: 1, DIE: 2, SHOOT: 3 },
    currentZombieState: null,
    sprite: null,
    count: 30,
    biteTime: 1, //her bir saniye için kullanılacak
    damage: 10,
    currentBiteTime: 0,
    zombies0: null,
    itemHealt: null,
    itemFiveSeven: null,
    itemAk47: null,
    itemXm1014: null,
    itemKey: null,
    sound: null,
    soundDie: null,
    speed: 40,
    boolSound: false,

    preload: function ()
    {

    },

    create: function () {
        this.zombies0 = new Array();
        _soundManager.zombieSoundArray = new Array();

        for (var i = 0; i < this.count; i++)
        {
            if (i === 0) {
                this.zombies0.push(new this.createZombie0(i, true, false, false, false));
            }
            else if (i === 1) {
                this.zombies0.push(new this.createZombie0(i, true, false, false, false));
            }
            else if (i === 2)
            {
                this.zombies0.push(new this.createZombie0(i, false, true, false, false));
            }
            else if (i === 3) {
                this.zombies0.push(new this.createZombie0(i, false, false, true, false));
            }
            else if (i === 4)
            {
                this.zombies0.push(new this.createZombie0(i, false, false, false, true));
            }
            else if (i === 5)
            {
                this.zombies0.push(new this.createZombie0(i, false, false, false, false,true));
            } else
            {
                this.zombies0.push(new this.createZombie0(i, false, false, false, false, true));
            }
        }

    },

    update: function ()
    {
        //zombilerin baktığı yön
        for (var i = 0; i < this.zombies0.length; i++)
        {
            if (this.zombies0[i].alive)
            {
                this.zombies0[i].sprite.rotation = game.physics.arcade.angleBetween(this.zombies0[i].sprite, _character.sprite);
                game.world.bringToTop(this.zombies0[i].sprite);
                //zombilerin takip ettiği karakter
                game.physics.arcade.moveToObject(this.zombies0[i].sprite, _character.sprite, this.speed);
                //zombilerin sesi
                if (this.distanceBetween(_character.placeHolder, this.zombies0[i].sprite) < 850 && this.zombies0[i].boolSound === false)
                {

                    _soundManager.zombieSoundArray[i].play();
                    this.zombies0[i].boolSound = true;

                } else if (this.distanceBetween(_character.placeHolder, this.zombies0[i].sprite) > 850 && this.zombies0[i].boolSound === true)
                {
                    _soundManager.zombieSoundArray[i].pause();
                    this.zombies0[i].boolSound = false;
                }

                if (this.distanceBetween(_character.placeHolder, this.zombies0[i].sprite) < 60)
                {
                    this.zombies0[i].sprite.body.velocity.setTo(0, 0);
                } else {
                    game.physics.arcade.moveToObject(this.zombies0[i].sprite, _character.sprite, this.zombies0[i].speed);
                }


            }         
        }
        
    },

    distanceBetween: function (source, target) {
        var x = source.x - target.x;
        var y = source.y - target.y;
        return Math.sqrt(x * x + y *y);
    },


    createZombie0: function (index,ihealth,iPistol, iAk47, iXm1014, key) {

        var x = game.world.randomX;
        var y = game.world.randomY;
        var randomSound = Math.floor((Math.random() * 2));
        _soundManager.zombieSounds(randomSound);
        this.health = 100;
        this.alive = true;

        this.boolSound = false;
        this.itemHealt = ihealth;
        this.itemFiveSeven = iPistol;
        this.itemAk47 = iAk47;
        this.itemXm1014 = iXm1014;
        this.itemKey = key,

        this.sprite = game.add.sprite(x, y, "zombie0");
        this.sprite.animations.add("die", [0]);
        this.sprite.animations.add("hit", [1, 2, 3, 4, 5]);
        this.sprite.animations.add("shoot", [6, 7, 8, 9, 10]);
        this.sprite.animations.add("walk", [11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25]);

        this.sprite.animations.play("walk", 10, true);

        this.sprite.anchor.setTo(0.5);
        this.sprite.scale.setTo(1.5);

        this.sprite.name = index.toString();
        game.physics.enable(this.sprite, Phaser.Physics.ARCADE);
        this.sprite.body.immovable = false;
        //this.sprite.body.collideWorldBounds = true;
        this.sprite.body.bounce.setTo(1);

        this.sprite.angle = game.rnd.angle();

        //rasgele yürüsünler
        game.physics.arcade.velocityFromRotation(this.sprite.rotation, 30, this.sprite.body.velocity);
    }

}
var _zombie0 = new Zombie0();