﻿var Zombie1 = function () { };

Zombie1.prototype = {
    health: null,
    player: null,
    alive: null,
    zombieStates: null,
    currentZombieState: null,
    sprite: null,
    count: 40,
    biteTime: 1, //her bir saniye için kullanılacak
    damage: 10,
    currentBiteTime: 0,
    zombies1: null,
    itemHealt: null,
    itemFiveSeven: null,
    itemAk47: null,
    itemXm1014: null,
    itemKey: null,
    speed: null,

    preload: function ()
    {

    },

    create: function ()
    {
        this.zombies1 = new Array();
        _soundManager.zombie1SoundArray = new Array();

        for (var i = 0; i < this.count; i++)
        {
            if (i === 0)
            {
                this.zombies1.push(new this.createZombie1(i, true, false, false, false));
            }
            else if (i === 1)
            {
                this.zombies1.push(new this.createZombie1(i, true, false, false, false));
            }
            else if (i === 2)
            {
                this.zombies1.push(new this.createZombie1(i, false, true, false, false));
            }
            else if (i === 3)
            {
                this.zombies1.push(new this.createZombie1(i, false, false, true, false));
            }
            else if (i === 4)
            {
                this.zombies1.push(new this.createZombie1(i, false, false, false, true));
            }
            else if (i === 5)
            {
                this.zombies1.push(new this.createZombie1(i, false, false, false, false, true));
            } else
            {
                this.zombies1.push(new this.createZombie1(i, false, false, false, false, true));
            }
        }

    },

    update: function ()
    {
        
        for (var i = 0; i < this.zombies1.length; i++)
        {
            if (this.zombies1[i].alive)
            {
                //zombilerin baktığı yön
                this.zombies1[i].sprite.rotation = game.physics.arcade.angleBetween(this.zombies1[i].sprite, _character.sprite);
                game.world.bringToTop(this.zombies1[i].sprite);
                //zombilerin takip ettiği karakter
                if (this.distanceBetween(_character.placeHolder, this.zombies1[i].sprite) < 790 && this.distanceBetween(_character.placeHolder, this.zombies1[i].sprite) > 60)
                {
                    this.zombies1[i].currentZombieState = this.zombies1[i].zombieStates.WALK;
                    if (this.zombies1[i].currentZombieState === this.zombies1[i].zombieStates.WALK )
                    {
                        if (this.zombies1[i].boolSound === false) {
                            _soundManager.zombie1SoundArray[i].play();
                            this.zombies1[i].boolSound = true;
                        }
                        
                        game.physics.arcade.moveToObject(this.zombies1[i].sprite, _character.sprite, this.zombies1[i].speed);
                        this.zombies1[i].sprite.animations.play("z1walk", 17, true);
                    }
                }
               
                else if (this.distanceBetween(_character.placeHolder, this.zombies1[i].sprite) < 61)
                {

                    this.zombies1[i].currentZombieState = this.zombies1[i].zombieStates.SHOOT;
                    if (this.zombies1[i].currentZombieState === this.zombies1[i].zombieStates.SHOOT)
                    {
                        this.zombies1[i].sprite.body.velocity.setTo(0, 0);
                        this.zombies1[i].sprite.animations.play("z1attack", 9, true);
                    }
                }
                else
                {
                    this.zombies1[i].currentZombieState = this.zombies1[i].zombieStates.IDLE;
                    if (this.zombies1[i].currentZombieState === this.zombies1[i].zombieStates.IDLE )
                    {
                        if (this.zombies1[i].boolSound === true) {
                            _soundManager.zombie1SoundArray[i].pause();
                            this.zombies1[i].boolSound = false;
                            console.log("sus " + this.zombies1[i].sprite.name);
                        }
                        
                        this.zombies1[i].sprite.body.velocity.setTo(0, 0);
                        this.zombies1[i].sprite.animations.play("z1idle", 16, true);
                    }

                }

            } else {
                if (this.zombies1[i].boolSound === true)
                {
                    _soundManager.zombie1SoundArray[i].pause();
                    this.zombies1[i].boolSound = false;
                    console.log("sus " + this.zombies1[i].sprite.name);
                }
            }
        }

    },

    distanceBetween: function (source, target)
    {
        var x = source.x - target.x;
        var y = source.y - target.y;
        return Math.sqrt(x * x + y * y);
    },


    createZombie1: function (index, ihealth, iPistol, iAk47, iXm1014, key)
    {

        var x = game.world.randomX;
        var y = game.world.randomY;
        var randomSound = Math.floor((Math.random() * 2));
        _soundManager.zombie1Sounds(randomSound);
        this.health = 200;
        this.alive = true;
        this.speed = 65;
        this.zombieStates = { IDLE: 0, WALK: 1, DIE: 2, SHOOT: 3 };
        this.currentZombieState = this.zombieStates.IDLE;
        this.boolSound = false;
        this.itemHealt = ihealth;
        this.itemFiveSeven = iPistol;
        this.itemAk47 = iAk47;
        this.itemXm1014 = iXm1014;
        this.itemKey = key,

        this.sprite = game.add.sprite(x, y, "zombie1");
        this.sprite.animations.add("z1attack", [0, 1, 2, 3, 4, 5, 6, 7, 8]);
        this.sprite.animations.add("z1idle", [9, 10, 18,, 19, 20, 21, 22, 23, 24, 25, 11, 12, 13, 14, 15, 16]);
        this.sprite.animations.add("z1walk", [26, 27, 35, 36, 37, 38, 39, 40, 41, 42, 28, 29, 30, 31, 32, 33, 34]);

        this.sprite.animations.play("z1idle", 10, true);

        this.sprite.anchor.setTo(0.5);
        //this.sprite.scale.setTo(0.5);

        this.sprite.name = index.toString();
        game.physics.enable(this.sprite, Phaser.Physics.ARCADE);
        this.sprite.body.immovable = false;
        //this.sprite.body.collideWorldBounds = true;
        this.sprite.body.bounce.setTo(1);

        this.sprite.angle = game.rnd.angle();

        //rasgele yürüsünler
        //game.physics.arcade.velocityFromRotation(this.sprite.rotation, 30, this.sprite.body.velocity);
    }

}
var _zombie1 = new Zombie1();