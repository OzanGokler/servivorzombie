﻿var SoundManager = function () { }

SoundManager.prototype =
{
    startSound: null,

    pistolShootSound: null,
    pistolReloadSound: null,

    Ak47ShotSound: null,
    Ak47ReloadSound: null,

    shotGunShotSound: null,
    shotGunShotSound2: null,
    shotGunReloadReload: null,

    doorOpenSound: null,
    doorLockSound: null,

    levelMusicSound: null,
    bossMusicSound: null,
    boss1MusicSound: null,
    boss2MusicSound: null,

    zombieSoundArray: null,
    zombie1SoundArray: null,

    charSoundArray: null,
   

    footGrassSound: null,
    footRockSound: null,
    footRock2Sound: null,

    charDieSound: null,
    gameOverSound: null,
    gameFinishSound: null,

    startMusic: function ()
    {

        this.startSound = game.add.audio("music_start");
        this.startSound.loop = true;
        this.startSound.play();
    },

    pistolShot: function() {
       
        this.pistolShootSound = game.add.audio("pistolShot");
        this.pistolShootSound.play();
    },

    pistolReload: function() {
        this.pistolReloadSound = game.add.audio("pistolReload");
        this.pistolReloadSound.play();
    },

    ak47Shot: function ()
    {
        this.Ak47ShotSound = game.add.audio("Ak47Shot");
        this.Ak47ShotSound.play();
    },

    ak47Reload: function ()
    {
        this.Ak47ReloadSound = game.add.audio("Ak47Reload");
        this.Ak47ReloadSound.play();
    },

    shotGunShot: function ()
    {
        this.shotGunShotSound = game.add.audio("ShotGunShot");
        this.shotGunShotSound.play();
    },

    shotGunShot2: function ()
    {
        this.shotGunShotSound2 = game.add.audio("ShotGunShot2");
        this.shotGunShotSound2.play();
    },

    shotGunReload: function ()
    {
        this.shotGunReloadReload = game.add.audio("ShotGunReload");
        this.shotGunReloadReload.play();
    },
    openDoor: function() {
        this.doorOpenSound = game.add.audio("DoorOpen");
        this.doorOpenSound.play();
    },
    lockDoor: function ()
    {
        this.doorLockSoundSound = game.add.audio("DoorLock");
        this.doorLockSoundSound.play();
    },
    levelMusic: function() {
        this.levelMusicSound = game.add.audio("music_level");
        this.levelMusicSound.loop = true;
        this.levelMusicSound.play();
    },
    levelBoss: function ()
    {
        this.bossMusicSound = game.add.audio("music_boss");       
        this.bossMusicSound.loop = true;
        this.bossMusicSound.play();
    },

    levelBoss1: function ()
    {
        this.boss1MusicSound = game.add.audio("music_boss1");
        this.boss1MusicSound.loop = true;
        this.boss1MusicSound.play();
    },

    levelBoss2: function ()
    {
        this.boss2MusicSound = game.add.audio("music_boss2");
        this.boss2MusicSound.loop = true;
        this.boss2MusicSound.play();
    },

    zombieSounds: function (index)
    {
        switch (index) {
            case 0:               
                var zombie0Sound = game.add.audio("zombieSound0");
                zombie0Sound.loop = true;
                zombie0Sound._volume = 0.4;                
                this.zombieSoundArray.push(zombie0Sound);
               
                break;
            case 1:
                
                var zombie1Sound = game.add.audio("zombieSound1");
                zombie1Sound.loop = true;
                zombie1Sound._volume = 0.6;
                this.zombieSoundArray.push(zombie1Sound);
                break;
        }
    },
    stopAllZombiesSound: function () {
        
        if (this.zombieSoundArray !== null)
        {
            for (var i = 0; i < this.zombieSoundArray.length; i++)
            {
                _soundManager.zombieSoundArray[i].destroy();
            }
        }
       
    },

    zombie1Sounds: function (index)
    {
        switch (index)
        {
            case 0:
                var zombie0Sound = game.add.audio("zombieSound0");
                zombie0Sound.loop = true;
                zombie0Sound._volume = 0.4;
                this.zombie1SoundArray.push(zombie0Sound);

                break;
            case 1:

                var zombie1Sound = game.add.audio("zombieSound1");
                zombie1Sound.loop = true;
                zombie1Sound._volume = 0.6;
                this.zombie1SoundArray.push(zombie1Sound);
                break;
        }
    },
    stopAllZombies1Sound: function () {
        if (this.zombie1SoundArray !== null)
        {
            for (var i = 0; i < this.zombie1SoundArray.length; i++)
            {
                _soundManager.zombie1SoundArray[i].destroy();
            }
        }

       
    },


    foot: function() {
        if (_character.currentLevel === "level") {
            this.footGrassSound = game.add.audio("footgrass");
            this.footGrassSound.play();
        }
        if (_character.currentLevel === "level1")
        {
            this.footRockSound = game.add.audio("footrock");
            this.footRockSound.play();
            
        }
        if (_character.currentLevel === "level2")
        {
            this.footRock2Sound = game.add.audio("footrock2");
            this.footRock2Sound.play();

        }
    },

    charSoundCreate: function () {
        var char_bite0 = game.add.audio("char_bite0");
        this.charSoundArray.push(char_bite0);

        var char_bite1 = game.add.audio("char_bite1");
        this.charSoundArray.push(char_bite1);

        var char_bite2 = game.add.audio("char_bite2");
        this.charSoundArray.push(char_bite2);

        console.log(this.charSoundArray);
    },

    charDie: function() {
        this.charDieSound = game.add.audio("char_die");
        this.charDieSound.play();
    },
    gameOver: function ()
    {
        this.gameOverSound = game.add.audio("music_gameOver");
        this.gameOverSound.play();
    },
    gameFinish: function ()
    {
        this.gameFinishSound = game.add.audio("music_finish");
        this.gameFinishSound.play();
    }


}
var _soundManager = new SoundManager();