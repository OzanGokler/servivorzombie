﻿var GameManager3 = function () { };

GameManager3.prototype = {

    preload: function ()
    {
        console.log("preload");
        _controller.preload();
    },

    create: function ()
    {
        console.log("create");
        _levelBoss1.create();
        _character.create();
        _boss1.create();

    },

    update: function ()
    {
        _controller.update();
        _character.update();
        _collisionHandler.update();
        _boss1.update();
       
    },
    render: function ()
    {
        _debug.look();
    }
}

var _gameManager3 = new GameManager3();