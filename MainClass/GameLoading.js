﻿var GameLoading = function () { };
GameLoading.prototype = {

    loadAssets: function () {
       
        //IMAGES
        game.load.image("survivorZombie", "Assets/survivorZombie.png");
        game.load.image("resume", "Assets/resume.png");
        game.load.image("pressTwo", "Assets/pressTwo.png");
        game.load.image("gameOver", "Assets/gameOverLogo.png");
        game.load.image("startScreen", "Assets/startScreen.jpg");
        game.load.image("bg_level", "Assets/bg_level.jpg");
        game.load.image("bg_level1", "Assets/bg_level1.jpg");
        game.load.image("bg_level2", "Assets/bg_level2.jpg");
        game.load.image("bg_boss", "Assets/bg_boss.jpg");
        game.load.image("bg_boss1", "Assets/bg_boss1.jpg");
        game.load.image("ammoFlash", "Assets/ammoFlash.png");
        game.load.image("fiveSevenBullet", "Assets/FiveSeven/FiveSevenBullet.png");
        game.load.image("ak47Bullet", "Assets/Ak47/Ak47Bullet.png");
        game.load.image("xm1014Bullet", "Assets/Xm1014/Xm1014Bullet.png");
        game.load.image("placeHolder", "Assets/placeHolder.png");
        game.load.image("dream", "Assets/dream.png");
        game.load.image("wakeUp", "Assets/wakeUp.png");

        //Items
        game.load.image("healthItem", "Assets/Items/healthItem.png");
        game.load.image("fiveSevenItem", "Assets/Items/fiveSevenItem.png");
        game.load.image("ak47Item", "Assets/Items/ak47Item.png");
        game.load.image("xm1014Item", "Assets/Items/xm1014Item.png");
        game.load.image("keyItem", "Assets/Items/keyItem.png");
    
        //ANIMATIONS ATLAS
        //AK47
        game.load.atlasJSONArray("ak47",
           ("Assets/Ak47/Ak47.png"),
           ("Assets/Ak47/Ak47.json"));
        //FIVESEVEN
         game.load.atlasJSONArray("fiveseven",
            ("Assets/FiveSeven/FiveSeven.png"),
            ("Assets/FiveSeven/FiveSeven.json"));
        //XM1014
         game.load.atlasJSONArray("xm1014",
            ("Assets/Xm1014/Xm1014.png"),
            ("Assets/Xm1014/Xm1014.json"));

        //Zombioe0
         game.load.atlasJSONArray("zombie0",
            ("Assets/Zombies/Zombie0/zombie0.png"),
            ("Assets/Zombies/Zombie0/zombie0.json"));
        //Zombioe1
         game.load.atlasJSONArray("zombie1",
            ("Assets/Zombies/Zombie1/zombie1.png"),
            ("Assets/Zombies/Zombie1/zombie1.json"));
         game.load.atlasJSONArray("blood",
             ("Assets/Zombies/Zombie1/blood.png"),
             ("Assets/Zombies/Zombie1/blood.json"));
        //Door
         game.load.atlasJSONArray("door",
             ("Assets/Door/door.png"),
             ("Assets/Door/door.json"));
        //Flame
         game.load.atlasJSONArray("flame",
             ("Assets/Flame/flame.png"),
             ("Assets/Flame/flame.json"));

        //Boss0
         game.load.image("boss0", "Assets/Boss/Boss0/boss0.png");
         game.load.image("boss1", "Assets/Boss/Boss1/boss1.png");
         game.load.image("boss2", "Assets/Boss/Boss2/boss2.png");

        //SOUNDS
        //pistol
         game.load.audio("pistolShot", "Assets/audio/Pistol/shot.wav");
         game.load.audio("pistolReload", "Assets/audio/Pistol/reload.mp3");

        //Ak47
         game.load.audio("Ak47Shot", "Assets/audio/Ak47/shot.wav");
         game.load.audio("Ak47Reload", "Assets/audio/Ak47/reload.mp3");

        //shotgun
         game.load.audio("ShotGunShot", "Assets/audio/ShotGun/shot.wav");
         game.load.audio("ShotGunShot2", "Assets/audio/ShotGun/shot2.wav");
         game.load.audio("ShotGunReload", "Assets/audio/ShotGun/reload.wav");

        //Door
         game.load.audio("DoorLock", "Assets/audio/Door/doorLock.wav");
         game.load.audio("DoorOpen", "Assets/audio/Door/doorOpen.wav");

        //Zombie
         game.load.audio("zombieDie", "Assets/audio/Zombie/zombieDieSound.wav");
         game.load.audio("zombieSound0", "Assets/audio/Zombie/zombieSound0.wav");
         game.load.audio("zombieSound1", "Assets/audio/Zombie/zombieSound1.wav");

        //Music
         game.load.audio("music_start", "Assets/Music/music_start.mp3");
         game.load.audio("music_boss", "Assets/Music/music_boss.mp3");
         game.load.audio("music_boss1", "Assets/Music/music_boss1.mp3");
         game.load.audio("music_boss2", "Assets/Music/music_boss2.mp3");
         game.load.audio("music_gameOver", "Assets/Music/music_gameOver.mp3");
         game.load.audio("music_finish", "Assets/Music/music_finish.mp3");

        //foot sound
         game.load.audio("footgrass", "Assets/audio/Foot/foot_grass.wav");
         game.load.audio("footrock", "Assets/audio/Foot/foot_rock.mp3");
         game.load.audio("footrock2", "Assets/audio/Foot/foot_rock2.mp3");
        //char sound
         game.load.audio("char_bite0", "Assets/audio/Char/char_bite0.mp3");
         game.load.audio("char_bite1", "Assets/audio/Char/char_bite1.mp3");
         game.load.audio("char_bite2", "Assets/audio/Char/char_bite2.mp3");
         game.load.audio("char_die", "Assets/audio/Char/char_die.mp3");
    },

    loadScripts: function ()
    {
        game.load.script("Controller", "Class/Controller.js");
        game.load.script("BackToGameMenu", "BackToGameMenu/BackToGameMenu.js");
        game.load.script("GameStarting", "MainClass/GameStarting.js");
        game.load.script("GameOver", "MainClass/GameOver.js");
        game.load.script("GameFinish", "MainClass/GameFinish.js");
        game.load.script("GameManager", "MainClass/GameManager.js");
        game.load.script("GameManager1", "MainClass/GameManager1.js");
        game.load.script("GameManager2", "MainClass/GameManager2.js");
        game.load.script("GameManager3", "MainClass/GameManager3.js");
        game.load.script("GameManager4", "MainClass/GameManager4.js");
        game.load.script("GameManager5", "MainClass/GameManager5.js");
        
        game.load.script("SoundManager", "Class/SoundManager.js");
        game.load.script("Character", "Class/Character.js");
        game.load.script("FiveSeven", "Class/Guns/FiveSeven.js");
        game.load.script("Ak47", "Class/Guns/Ak47.js");
        game.load.script("Xm1014", "Class/Guns/Xm1014.js");
        game.load.script("CharacterAnimationManager", "Class/CharacterAnimationManager.js");
        game.load.script("Zombie0", "Class/Zombies/Zombie0.js");
        game.load.script("Zombie", "Class/Zombies/Zombie1.js");
        game.load.script("CollisionHandler", "Class/CollisionHandler.js");
        game.load.script("Level", "Class/Levels/Level.js");
        game.load.script("Level1", "Class/Levels/Level1.js");
        game.load.script("Level2", "Class/Levels/Level2.js");
        game.load.script("LevelBoss", "Class/Levels/LevelBoss.js");
        game.load.script("LevelBoss1", "Class/Levels/LevelBoss1.js");
        game.load.script("LevelBoss2", "Class/Levels/LevelBoss2.js");
        game.load.script("Items", "Class/Items.js");
        game.load.script("Debug", "Class/Debug.js");
        game.load.script("Boss", "Class/Boss/Boss0.js");
        game.load.script("Boss1", "Class/Boss/Boss1.js");
        game.load.script("Boss2", "Class/Boss/Boss2.js");
    },

    StartGameManager: function () {
        game.state.add(gameStates.GameStarting, _gameStarting);
        game.state.add(gameStates.GameManager, _gameManager);
        game.state.add(gameStates.GameManager1, _gameManager1);
        game.state.add(gameStates.GameManager2, _gameManager2);
        game.state.add(gameStates.GameManager3, _gameManager3);
        game.state.add(gameStates.GameManager4, _gameManager4);
        game.state.add(gameStates.GameManager5, _gameManager5);
        game.state.add(gameStates.GameOver, _gameOver);
        game.state.add(gameStates.GameFinish, _gameFinish);

        game.state.start(gameStates.GameStarting);
    },

    preload: function () {
        this.loadScripts();
        this.loadAssets();
    },

    create: function () {
        this.StartGameManager();
    }
};
