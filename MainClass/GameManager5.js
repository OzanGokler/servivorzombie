﻿var GameManager5 = function () { };

GameManager5.prototype = {

    preload: function ()
    {
        console.log("preload");
        _controller.preload();
    },

    create: function ()
    {
        console.log("create");
        _levelBoss2.create();
        _character.create();
        _boss2.create();
    },

    update: function ()
    {
        _controller.update();
        _character.update();
        _collisionHandler.update();
        _boss2.update();     
    },
    render: function ()
    {
        _debug.look();
    }
}

var _gameManager5 = new GameManager5();