﻿var GameManager2 = function () { };

GameManager2.prototype = {

    preload: function ()
    {
        console.log("preload");
        _controller.preload();
    },

    create: function ()
    {
        console.log("create");
        _level1.create();
        _character.create();
        _zombie1.create();
    },

    update: function ()
    {
        _controller.update();
        _character.update();
        _collisionHandler.update();
        _zombie1.update();
    },
    render: function ()
    {
        _debug.look();
    }
}

var _gameManager2 = new GameManager2();