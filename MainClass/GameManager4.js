﻿var GameManager4 = function () { };

GameManager4.prototype = {

    preload: function ()
    {
        console.log("preload");
        _controller.preload();
    },

    create: function ()
    {
        console.log("create");
        _level2.create();
        _character.create();
        _zombie0.create();
        _zombie1.create();

    },

    update: function ()
    {
        _controller.update();
        _character.update();
        _collisionHandler.update();
        _zombie0.update();
        _zombie1.update();
        
       
    },
    render: function ()
    {
        _debug.look();
    }
}

var _gameManager4 = new GameManager4();