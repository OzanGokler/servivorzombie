﻿var gameWidth = 800;
var gameHeight = 480;
var game = new Phaser.Game(gameWidth, gameHeight, Phaser.CANVAS, "Survivor Zombie"
    /*{ preload: preload, create: create, update: update }*/);
var gameStates = {
    GameManager: "gameManager",
    GameManager1: "gameManager1",
    GameManager2: "gameManager2",
    GameManager3: "gameManager3",
    GameManager4: "gameManager4",
    GameManager5: "gameManager5",
    GameLoading: "gameLoading",
    GameStarting: "gameStarting",
    GameFinish: "gameFinish",
    GameOver: "gameOver"
};


Starter = function () { };
Starter.prototype =
{

    preload: function () {
        game.load.script(gameStates.GameLoading, "MainClass/GameLoading.js");
    },
    create: function ()
    {
        //game.forceSingleUpdate = true;
        game.world.setBounds(0,0, 2500, 2500);
        game.state.add(gameStates.GameLoading, GameLoading);
        game.state.start(gameStates.GameLoading);
        
    }
};
game.state.add("Starter", Starter);
game.state.start("Starter");