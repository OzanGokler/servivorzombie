﻿var GameStarting = function () { };

GameStarting.prototype = {
    bg: null,
    char: null,
    boolBegin: false,
    survivorZombie: null,
    pressB: null,

    blood: null,
    blood1: null,
    blood2: null,
    blood3: null,
    blood4: null,
    blood5: null,
    bloodTime: 0,
    bloodTime1: 0,
    bloodTime2: 0,
    bloodTime3: 0,
    bloodTime4: 0,
    bloodTime5: 0,
    

    preload: function ()
    {
        _controller.preload();
    },

    create: function ()
    {
        
        this.bg = game.add.sprite(0, 20, "startScreen");
        this.bg.scale.setTo(0.42);
        this.char = game.add.sprite(180, 190, "fiveseven");
        this.char.scale.setTo(2);
        _soundManager.startMusic();

        this.survivorZombie = game.add.sprite(250, 80, "survivorZombie");
        this.survivorZombie.anchor.setTo(0.5);
        this.survivorZombie.scale.setTo(0.5);

        this.pressB = game.add.sprite(250, 400, "pressTwo");
        this.pressB.anchor.setTo(0.5);
        this.pressB.scale.setTo(0.5);

        var tweenPressB = game.add.tween(this.pressB.scale).to({ x: 0.8, y: 0.8 }, 1000, Phaser.Easing.Linear.Out, true).loop(true);

        //  And this tells it to yoyo, i.e. fade back to zero again before repeating.
        //  The 3000 tells it to wait for 3 seconds before starting the fade back.
        tweenPressB.yoyo(true);

        this.boolBegin = false;
    },

    update: function () {
        this.bloods();
        _controller.update();

        if (_controller.buttonB_isDown && this.boolBegin === false) {
            this.boolBegin = true;
            _soundManager.openDoor();
            game.camera.fade(0x000000, 2000);
            game.camera.onFadeComplete.add(function () {
                _soundManager.startSound.stop();
                game.state.start(gameStates.GameManager);
            }, this);
            
        }
    },

    bloods: function ()
    {
        if (this.bloodTime + 1 < game.time.totalElapsedSeconds())
        {
            this.blood = game.add.sprite(130, 180, "blood");
            this.blood.anchor.setTo(0.5);
            this.blood.animations.add("die", [0, 1, 2, 3, 4, 5]);
            this.blood.animations.play("die", 12).onComplete.addOnce(this.bloodComplete, this);;

            this.bloodTime = game.time.totalElapsedSeconds();
        }

        if (this.bloodTime1 + 1.5 < game.time.totalElapsedSeconds())
        {
            this.blood1 = game.add.sprite(350, 180, "blood");
            this.blood1.anchor.setTo(0.5);
            this.blood1.animations.add("die", [0, 1, 2, 3, 4, 5]);
            this.blood1.animations.play("die", 12).onComplete.addOnce(this.blood1Complete, this);;

            this.bloodTime1 = game.time.totalElapsedSeconds();
        }

        if (this.bloodTime2 + 0.8 < game.time.totalElapsedSeconds())
        {
            this.blood2 = game.add.sprite(230, 450, "blood");
            this.blood2.anchor.setTo(0.5);
            this.blood2.scale.setTo(0.5);
            this.blood2.animations.add("die", [0, 1, 2, 3, 4, 5]);
            this.blood2.animations.play("die", 12).onComplete.addOnce(this.blood2Complete, this);;

            this.bloodTime2 = game.time.totalElapsedSeconds();
        }

        if (this.bloodTime3 + 1.2 < game.time.totalElapsedSeconds())
        {
            this.blood3 = game.add.sprite(520, 390, "blood");
            this.blood3.anchor.setTo(0.5);
            this.blood3.scale.setTo(1.5);
            this.blood3.animations.add("die", [0, 1, 2, 3, 4, 5]);
            this.blood3.animations.play("die", 12).onComplete.addOnce(this.blood3Complete, this);;

            this.bloodTime3 = game.time.totalElapsedSeconds();
        }

        if (this.bloodTime4 + 1.4 < game.time.totalElapsedSeconds())
        {
            this.blood4 = game.add.sprite(80, 350, "blood");
            this.blood4.anchor.setTo(0.5);
            this.blood4.scale.setTo(0.7);
            this.blood4.animations.add("die", [0, 1, 2, 3, 4, 5]);
            this.blood4.animations.play("die", 12).onComplete.addOnce(this.blood4Complete, this);;

            this.bloodTime4 = game.time.totalElapsedSeconds();
        }

        if (this.bloodTime5 + 1.7 < game.time.totalElapsedSeconds())
        {
            this.blood5 = game.add.sprite(600, 50, "blood");
            this.blood5.anchor.setTo(0.5);
            this.blood5.scale.setTo(1.3);
            this.blood5.animations.add("die", [0, 1, 2, 3, 4, 5]);
            this.blood5.animations.play("die", 12).onComplete.addOnce(this.blood5Complete, this);;

            this.bloodTime5 = game.time.totalElapsedSeconds();
        }
    },

    bloodComplete: function() {
        game.add.tween(this.blood).to({ alpha: 0 }, 250, Phaser.Easing.Linear.None, true);
    },
    blood1Complete: function ()
    {
        game.add.tween(this.blood1).to({ alpha: 0 }, 250, Phaser.Easing.Linear.None, true);
    },
    blood2Complete: function ()
    {
        game.add.tween(this.blood2).to({ alpha: 0 }, 250, Phaser.Easing.Linear.None, true);
    },
    blood3Complete: function ()
    {
        game.add.tween(this.blood3).to({ alpha: 0 }, 250, Phaser.Easing.Linear.None, true);
    },
    blood4Complete: function ()
    {
        game.add.tween(this.blood4).to({ alpha: 0 }, 250, Phaser.Easing.Linear.None, true);
    },
    blood5Complete: function ()
    {
        game.add.tween(this.blood5).to({ alpha: 0 }, 250, Phaser.Easing.Linear.None, true);
    },
}

var _gameStarting = new GameStarting();