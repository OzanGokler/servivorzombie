﻿var GameManager = function () { };

GameManager.prototype = {

    preload: function () {
        _controller.preload();
        
    },

    create: function ()
    {
        //ARCADE PHYSICS START
        //game.physics.startSystem(Phaser.Physics.ARCADE);
        _level.create();
        _character.create();
        _zombie0.create();
    },

    update: function() {
        _controller.update();
        _character.update();
        _collisionHandler.update();
        _zombie0.update();
    },
    render: function() {
        _debug.look();
    }
}

var _gameManager = new GameManager();