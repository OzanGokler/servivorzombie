﻿var GameManager1 = function () { };

GameManager1.prototype = {

    preload: function () {
        console.log("preload");
        _controller.preload();
    },

    create: function ()
    {
        console.log("create");
        _levelBoss.create();
        _character.create();
        _boss0.create();
    },

    update: function ()
    {
        _controller.update();
        _character.update();
        _collisionHandler.update();
        _boss0.update();

    },
    render: function ()
    {
         _debug.look();
    }
}

var _gameManager1 = new GameManager1();